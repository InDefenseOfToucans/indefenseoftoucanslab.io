---
layout: post
author:
- InDefenseOfToucans
bibliography:
- sources.bib
date: February 2021
title: 'The Myth of the Trotskyist to Neo-Conservative Pipeline'
---

Introduction
============

If you have been around in internet leftist circles for any amount of
time you have probably seem memes mention the \"Trot to neocon
pipeline\" or that neo-conservatism came out of Trotskyism. This works
its way into more serious takes, I think most people who repeat this
have never looked into it at all and are just repeating memes.

The typical claim is some Trotskyists split off and became conservatives
but they kept some of their Trotskyist opinions like Permanent
Revolution which people think means invading other countries to spread
the revolution, and so therefore these Trots turned Conservative who
created Neo-Conservatism were for invading countries to spread
conservatism and liberal democracy, and therefore Trots are responsible
for the Iraq war or the Gulf War or whatever.

This myth came out of the 1980s US where Palecons and Neo-Cons were
engaging in debates and polemics, and this myth was created by paleocons
to slander the neocons, but it has been spread a lot further then just
Paleocons

Now for this video I am basically pulling all of this from William F
King's Neoconservatives and \"Trotskyism\" published in the American
Communist History which is a peer-reviewed journal focusing on the
historical impact of Communism in the United States, if you want a more
in depth breakdown please go read his article.

The Claims
==========

So first lets talk about the claims, well I did some digging on the
internet to find examples of what the most modern claims are about this
so I can target those.

The most common I found was something along the lines of well former
Trotskyists founded Neo Conservatism, so that's the main claim I am
going to be dealing with. Most of these are taken from Reddit.

This one from an upvoted post on r/communism101 \"The route from Trotsky
to Neo-conservatism was mostly that of the Jewish, New York
intellectuals.

I actually managed to lose track of where this post is from. \"The
entire neo-con movement was founded by former Trots\"

From the New York Times in 1998

\"Nathan Glazer has had more second thoughts in his lifetime than most
people have had thoughts. He is the most modest (and the least
tendentious) of the brilliant Jewish boys who attended City College 60
years ago and later came to be known collectively as the New York
Intellectuals. The City College crowd helped chart postwar America's
ideological frontier; like many of his classmates, Glazer journeyed
steadily rightward for decades, from Trotskyist to social democrat to
neo-conservative. \"

https://www.nytimes.com/1998/06/28/magazine/nathan-glazer-changes-his-mind-again.html

Founding of Neo-Conservatism and the Origins of the Myth
========================================================

So first it is probably best to establish a bit of history what even is
neo-conservatism, and this is hard as it was not the self descriptions
of a group of people, but something applied to various figures. After
spending a few days reading about it there seems to be a lot of
inconsistency. The exact start date does not even seem to be agreed
upon. This actually makes this sorta of theory hard to debunk, because
people can sorta of arbitrary include who is a NeoCon. But I will first
cover what I think the best way of defining the group is and then deal
with some alternatives.

They hated the New Left and Counterculture and the Vietnam protests.
They didn't think the market was moral and humans had to enforce that
and generally were critical of unrestrained capitalism, not that they
didn't love capitalism, just more they would take issues with the idea
of people like Hayek, and wouldn't agree with someone who held more
libertarian views, they felt the government had a role to play. They
didn't reject the New Deal or even some parts of the Great Society, this
is the main aspect of what made this group different.

Nathan Glazer who is considered one of the First NeoConseravtives talked
about the group like this

\"All of us had voted for Lyndon Johnson in 1964, for Hubert Humphrey in
1968 .. continued to vote for Democratic presidential candidates all the
way to the present\"

\"Had we not defended the major social programs, from Social Security to
Medicare, there would have been no need for the \"neo\" before
conservatism\"\"

We should also talk about where exactly did these people originate from,
well they were built up of really two groups, a section of the New York
Intellectuals, this would be Irving Kristol, Nathan Glazer(who I might
add didn't like the label of neocon) who are typically the NeoCons from
a more leftist background, and more hawkish Democrats like Elliott
Abrams, and Daniel Moynihan. [{% cite highhistoriographyneoconservatism %}
480-481]

But even that distinction is not that useful because while some parts of
who formed Neo Conservatism were part of the New York
Intellectuals(Don't conflate the two groups people often do, some
NeoCons were New York Intellectuals, but not all became Neocons) this
group did have a lot of Marxist influence for many of their time at the
City College of New York, this was during the 1930s, they had become
Liberals for the most part before and during WW2, I will get more into
specific figures and their histories a bit later. So fundamentally the
Neocons were Cold War Liberals and had been for decades become coming to
neo-conservatism. [{% cite king-neoconsandtrots %} 253-254]

So alongside these Neo Conservatives they started to fight with what has
been termed Paleoconservatives basically more \"old fashion\"
conservatives, they were very opposed to immigration, and you might say
well aren't all conservatives anti-immigrant, but you should remember
the last president to do full amnesty for undocumented immigrants was
Reagan, also this is all relative and in comparison to each other. So
there is certainly conservatives who support immigration from an
economic perspective. Where Neocons often were still for New Deal and
some Great Society programs the paleocons remained opposed to the New
Deal, and the Great Society. Where Neo Conservatives often claimed to
support the civil rights movement in part, though many argued some
aspects of it 'went too far', the paleocons full out rejected it. Again
it is all relative were talking here certainly they weren't exactly
progressive, but compared to other conservatives they were more for
these things. Another big difference is paleocons tend to be more
isolationist where neoconservatives, though this can't be universal and
members associated with neocons opposed the Gulf War at times.

Paleocons often argued against the Iraq war, for example Samuel T.
Francis in 2003 described the point of the war \"the point is to wipe
out Israel's enemies. \" and claimed describing the neocons \"Of course
the Likudniks don't care about American casualties very much.\" Charges
that neocons aren't loyal to America and are only loyal to Israel are
pretty common among Paleocons, he even calls them \"Likundniks\". You
can find a lot of this stuff and shouldn't be a shock that Paleocons are
often open white nationalists, they reject that the NeoCons are even
American and that they are loyal to Israel, which I would hope you all
recognize as ridiculous and bigoted, the Paleocons often think the US
has no logical reason for supporting Israel and that the fact that the
US government does is some conspiracy. I hope anyone would see how
ridiculous this is the American ruling class backs Israel because it is
in their interests in the region, it has nothing to do with Israel
Jewish, and supported by some Jewish Cabal is controlling the US. Just
in the same way there is not some Arab cabal controlling the white house
to make the US support Saudi Arabia.

This is important to keep in mind that Paleocons often go down the road
of anti-semetic conspiracy theories and this ties into the whole
Trot-NeoCon thing which I will get into in a minute.

Creation of the Myth
--------------------

As noted by William F King, in 1988 the paleocon historian Paul
Gottfried authored \"The Conservative Movement\" which was supposed to
be a history of the conservative movement with a focus on the Neocon and
Paleocon divide, and there is 0 mention of some trotskyist connection in
the work, however in the 90s when he wrote a revived edition suddenly he
inserted all this Trotskyist connection. According to King the form of
this idea originated after the Gulf War. Though there is some indication
of it prior to the Gulf war in 1986 at a debate Stephen Tonsor
complained of \"former Marxists\" and that had Stalin not killed Trotsky
he would be working for the Hoover Institute. But this was more of a
claim or a joke made at a debate rather then in the more academic
histories of neocons produced by the Palecon movement.
[{% cite king-neoconsandtrots %} 250-253]

According to King the article that created it in the form we know today
that Neo Conservatism has Trotskyist roots was written for the
Washington Report on Middle East Affairs and was written by Leon Hadar
of the Cato Institute.

Let us take a look at these \"Trotskyists\"
===========================================

In this he argues the major figures in the Neocon movement were former
Trots who went to \"poor man's harvard\" CCNY, naming Irving Kristol,
Norman Podhoretz, Midge Decter, Nathan Glazer, Daniel Bell and Ben
Wattenberg.

However again as King points out there is some really huge issues with
this theory. Ben Wattenberg was 4 years old when Irving Kristol went to
CCNY, and Wattenberg never even attended CCNY, Wattenberg never was even
a leftist let alone a Trot he was a capitalist his whole life. Norman
Podhoretz and Midge Decter also never went to CCNY, or ever been
Marxists.

Nathan Glazer and Daniel Bell however actually did attend CCNY in the
30s, so ya know the article gets at least one thing right, but there is
more problems.

Nathan Glazer
-------------

Nathan Galzer was never a Trotskyist he was a Left Socialist-Zionist,
never a Trotskyist. Even the New York Times called him a Trotskyist, in
the article I pulled from earlier. I have seen several attempts to tie
Trotsky to zioinism which is really just antisemetic nonsense and
assuming he is a zionist because of his background.
https://www.nytimes.com/1998/06/28/magazine/nathan-glazer-changes-his-mind-again.html

Daniel Bell
-----------

Well what about Daniel Bell, well he was a member of the YPSL in the
early 30s, in 1936 the Trotskyist movement in the US decided to
implement the \"french turn\" in the US and enter into the Socialist
Party with the intention of breaking off the young radicals in the
party. [{% cite alexander1991international %} 785]

However while that was happening in part in reaction of the Trotskyists
entering the Socialist Party the Social Democratic Federation was formed
and Glazer became a member, then in Nathan Glazer also wrote
\"Trotskyism as a derivative of Leninism, is alien to freedom of thought
and conscience and must be fought\" in 1939 [{% cite king-neoconsandtrots %} 252]

Irving Kristol
--------------

So that leaves us just with Irving Kristol and I want to spend some
extra time on him specifically since he is probably your best argument
if your going to tie Trotskyism and Neo Conservatism together. He
actually wrote an autobiographical essay titled \"Memoirs of a
Trotskyist\" so hey that is something. In it he says

\"I was a member in good standing of the Young People's Socialist League
(Fourth International). This organization was commonly, and correctly,
designated as Trotskyist\"

However Irving really was not a full member of the YPSL he was a Fellow
Traveler, and in 1940 he did not join the Socialist Workers Party the
Trotskyist party in the US at the time, but the Workers Party led by Max
Shachtman. Which Trotsky described Schactman this way in 1940 in an
article titled \"Petty-Bourgeois Moralists and the Proletarian Party\"

and to read this I asked Socialist in a Barrel who is new to creating
videos, you should go subscribe to them and if you like my content you
will probably like either of these two videos \"One time Russian almost
got its own Latin alphabet, The Problems with soviets in the Media \"

\"Only the other day Shachtman referred to himself in the press as a
"Trotskyist." If this be Trotskyism then I at least am no Trotskyist.
With the present ideas of Shachtman, not to mention Burnham, I have
nothing in common. I used to collaborate actively with the New
International, protesting in letters against Shachtman's frivolous
attitude toward theory and his unprincipled concessions to Burnham, the
strutting petty-bourgeois pedant. But at the time both Burnham and
Shachtman were kept in check by the party and the International. Today
the pressure of petty-bourgeois democracy has unbridled them.\"

and of the Workers Party Kristol belonged to the \"Shermanites\" which
was a right wing faction of the party that eventually was thrown out in
1941 because they rejected Marxism. [{% cite king-neoconsandtrots %} 254-255]

Kristol is also quoted as having said

\"I was a young Trotskyist for 18 months or so \... but even when I was
in it, I couldn't quite take it seriously.\" ---Irving Kristol\"

Going to quote King's conclusion on Irving

\"Looking beyond both journalistic reminiscences and polemically
motivated exaggerations, a more balanced appraisal of Irving Kristol's
Trotskyism is that he was involved on the intellectually energetic
margins of the movement, and then briefly passed through the movement
itself while maintaining a non-Trot- skyist---and arguably, given the
Shermanites' emphasis on political democracy, a non-Marxist---political
outlook. As Kristol himself would remark in later years, \"I have never
considered myself to be an 'ex-Trotskyist' in the sense that some people
conceive of themselves as 'ex-communists'. The experience was never that
important to me \....\"\* By the end of the Second World War, during
which Kristol saw service with the US Army in Europe, he was no longer a
socialist of any stripe.\"

He is even more \"positive\" on Kristol's Trotskyism, he was in the
right wing of a party led by a man Trotsky declared to have nothing in
common with his ideas. I would say he never really was a Trotskyist.

Another thing to keep in mind when this was, this was 1930 and very
briefly into the very early 1940s, it would be another 30ish years
before NeoConservatism would be founded, I was not even alive 30 years
ago. That is a long time if you are in your 20s like me imagine you
changed political ideology then 30 years from now in 2050 you were part
of a movement of a new sorta ideology, it sounds pretty ridiculous to
act like your opinions you left behind in 2020 had some great impact on
your opinions in 2050s and 2060s.

What about the other neocons
============================

Alright lets maybe take a look at a larger list of NeoCons who could
maybe be listed to support this theory, see the issue most of the people
who talk about this theory never actually list names half the time, and
when they do well as you saw above only 1 of the people they listed even
flirted with Trotskyism and only for a moment decades before the
founding of Neo Conservatisim.

Jeane Kirkpatrick
-----------------

She was actually a member of the Socialist Party, however she was in the
1940s after all of the Trotskyists were out of the party.

See the Trotskyists were purged over the course of
1937/1938[{% cite alexander1991international %} 792]

Daniel Moynihan
---------------

Dude was just always a democrat to the best I can find out.

John Podhoretz
--------------

Born to Norman Podhoretz who was never a Marxist, and he was raised
conservative by his parents.

Seymour Lipset
--------------

Seymour Lipset joined the YPSL in highschool which would have been
before the Trotskyist entryism and following that he ended up also a
part of that Shermanite faction who really weren't so he was in a
similar camp to Irving Kristol a brief few month flirtation in the 30s,
he remained in the Socialist Party until 1960, so he never left with the
Trotskyists to the SWP implying he was never a Trotskyist.

Sidney Hook
-----------

Sidney Hook was actually a Marxist, he was a Marxist, he actually
studied at the Marx-Engels Institute in Moscow, and was a supporter of
the Communist Party and William Z. Foster until 1933. Then he ended up
being part of the American Workers Party which the early Trotskyist
movement merged with. However he never actually joined the Trotskyist
parties he remained a fellow traveler of the movement until 1939.
[{% cite king-neoconsandtrots %} 252]

Conclusion
----------

I could probably find more, but the thing is I would just start listing
a bunch of non Trotskyists, really only Irving and Lipset are the only
ones to have even touched Trotskyism there there are many numerous
others who never even Trots and some never even Marxists. Even if you
count Irving Kristols wife who also briefly flirted with Trotskyism that
brings it up what to 3, out of how many of this sorta first generation
Neo Conservatives.

Shachtmanism created Neo Conservatism
=====================================

So lets talk about another alternative theory that while maybe Neo
Conservatism was not created by Trots there is a whole second generation
of neocons created from Shachtmanism. Now there is a smidge of truth to
this one as a number of Neocons did come out of the Socialist Party who
were in leadership at the time Shachtman was in the party. However

Trotsky said that if Shachtman is a Trotskyist then he(being trotsky) is
not one. This was in 1940, not only that he his group was required to
declare they no longer followed his quasi-trotskyist ideology by the
time he joined [{% cite king-neoconsandtrots %} 257]

Also worth bringing up that people might find interesting a position he
came to and joined the SP to advocate for was for the idea of them
becoming a group that influenced the Democrats into pulling left
[{% cite alexander1991international %} 812-813]

You might recognize a name that was part of Shachtman's group and was
for pulling the Dems to the left Michael Harrington who been in
Shachtman's group who would eventually would form the Democratic
Socialist Organizing Committee, and eventually the Democratic Socialists
of America along with Irving Howe another follower of Shachtman. So ya a
few people who associated with Shactman did become neocons, this was
after really any claim of connection to Trotskyism were really gone.
More figures moved to form the DSA then became Neocons, the DSA has more
in common with Shachtman's legacy. So I don't think this really proves
any connection given Shactman was an odd person in the Trotskyist
movement and was disliked by Trotsky and when he joined the Socialist
Party he rejected his old ideology as part of the conditions, after that
a few people he had influence on became Neocons, so this is really
stretching to try and prove any connection.

Burnham the original Neocon
===========================

So another sort of \"idea\" i guess for how Neocons were founded is
pinning it on James Burnham he was the original Neocon not figures like
irving, that it was the William F. Buckley's national review not The
Public Interest and Commentary that sorta started Neo-Conservatism.

First a little bit of backstory of how Burnham ended up and his position
within the Trotskyist movement. I think best to maybe attempt to provide
a short summary of the American Trotskyist movement and how it came
about

I attempt to explain the history of American Trotskyism
=======================================================

First a note on the name of Trotskyism historically Trotskyists called
themselves Bolshevik-Leninists it is what Trotsky called his group, but
I use the term Trotskyists because it is what people know today.

Formation of the Communist Party
--------------------------------

So lets start with how the Communist Party came about. The Communist
Party originated within the Socialist Party, the Left Wing the party had
be helped along by the influence of the paper Novy Mir which both
Nikolai Bukharin and Leon Trotsky wrote for, Bukharin advocated for the
left wing of the Socialist Party to Split where Trotsky advocated more
for them to purge the party of opportunist elements[{% cite cohen1973bukharin %}
43-44] In January of 1917 a meeting was held in the flat of Ludwig Lore,
this meeting goal was to debate if to split and to try and unify the
left wing elements withing the Socialist Party. Bukharin, Kollontai and
Trotsky would all be there and debate the question, this meeting along
with Novy Mir on the whole played a part in Trotsky coming close to the
Bolsheviks. Though Trotsky's position apparently \"won\" the debate and
from this meeting would come the editorial board of \"Class Struggle\" a
journal to advocate for the left wing of the Socialist Party.
[{% cite zumoff2015communist %} 32-33]

Within the Left Wing of the Socialist Party there was two factions by
1919, on the whole they wanted to become part of the Third
International, but one side favored leaving the Socialist Party right
away, the other side wanting to avoid a premature split wanted to wait
and see if the left could take over the party at the convention, and
sought to bring Eugene V Debs to their side, two notable names of this
faction was John Reed and James P Cannon. They would fail to convince
Debs as he didn't want to take a position on the factional struggle.
Eventually parts of the group would be broken off and other parts were
expelled eventually they would form the Communist Labor Party and
attempt to influence to SP, but CLP were barred. Those that had pushed
for an immediate split had formed the Communist Party of America.
[{% cite zumoff2015communist %} 36-41] The typical way these groups are presented
is as \"immigrants\" vs \"indigenous Americans\", I really hate this way
of explaining it, as for one these people were settlers not indigenous,
and the immigrant group was pretty much just eastern European
immigrants, Asian immigrants avoided both groups. As well neither party
had a single black delegate at their founding conventions, there was
breakaways of black socialists away from the Socialist Party such as the
African Blood Brotherhood, they didn't join with either faction as they
felt neither took the issue of black liberation seriously
[{% cite foner1977american %} 305-311]

Both factions pledged support to the October Revolution and the Third
International, and both published a newspaper with the name Communist.
This nonsense would go on for some time and would disorganize US
Communism and cause a lot of the left wing of the Socialist party to
drift away, eventually the Comintern had enough of this and they made
John Reed of the CKP and John Anderson of the CPA sign an agreement to
merge the parties. This is how America got a unified Communist
Party.[{% cite zumoff2015communist %} 43-44]

Factional Struggles
-------------------

American Trotskyism has its roots in the faction of this Communist Party
led by James P. Cannon. Within the party at the time there was roughly
The William Z Foster Faction, Jay Lovestone, and The Cannon faction. The
Lovestone faction took control of the party in 1925 from the
Foster-Cannon Faction they were able to do this due to the support of
the Comintern, there would be various struggles within the party over
issues of the language federations and legalization during the whole of
the 20s, within the US the debates in the USSR between Trotsky's Left
Opposition and the Stalin-Bukharin faction were basically unknown.
Cannon tried to go to Moscow to gain some support from the Comintern
against the Lovestoneites, the differences were over focus with Foster
and Cannon favoring basing the party in Chicago and trade union work.
[{% cite alexander1991international %} 760-765]

Canon had previously gone to Moscow to get their support in 1922 when
the debate was over legalization of the party, as the party had been
operating as an underground party with a front party called The workers
Party. Cannon was part of the group that wanted to merge the underground
party into the legal party and become a fully legal party. The reason
people opposed it many felt only an underground party could be
revolutionary and that America would soon have its own October
Revolution and had no purpose for a legal party. Cannon went to Moscow
and met with Trotsky, and Trotsky promised to talk to Lenin and that
Lenin would support legalization [{% cite cannon2002history %} 40-43]

Cannon went to Moscow again in 1928 for the Sixth World Congress of the
Comintern, he was hoping for a repeat of 1922 and to get support for his
position. While there he was placed on the program commission, while
there someone in the Comintern had accidentally given Trotsky's document
\"The Draft Program of the Communist International: A Criticism of
fundamentals\" to the translators and it was translated into English and
mistakenly given it to James P Cannon, and Maurice Spencer. Cannon would
smuggle this out of Moscow and planned to advocate for Trotsky, but at
the time there was not a single member of the American Communist party
who was a support of Trotsky.[{% cite cannon2002history %} 76-49]

Ejection
--------

Cannon would recruit within his allies in the party, however as the
Fosterite faction became more aware of the Cannonite factions movement
towards Trotskyism they broke up their alliance, and eventually James
Cannon would make a public declaration of their 100 percent support of
the Russian Opposition.

Jay Lovestone who was Party secretary would carry out a purge of the
Trotskyists, he also had people led raids into the apartments of
Trotskyist leaders in the US, Trotskyists were attacked and beaten in
their meetings. All of these efforts were considered very aggressive and
members who refused to endorse these tactics were often removed from the
party, this caused many members in Minneapolis to be removed despite not
being favorable to Trotsky which drove them into the arms of the
Trotskyists.

Once removed the Trotskyists set out to start publishing a newspaper and
they did The Militant began publishing in November 1928. They also found
some allies who were followers of Bordiga. Eventually this would result
int he formation of the Communist League of America, Left Opposition of
the Communist Party, they held their conference in Chicago in May of
1929 it had to be protected by coal miners and IWW members to prevent it
from being attacked by the Communist Party members.

The position at the time of the Trotskyists was that they were an
opposition group of the Communist Party within the Comintern fighting
for reform within the Comintern and within the Soviet Union.

During this time they ran into several issues, one of which was a lot of
people joined for the wrong reasons, not because they were Trotskyists
but they were bitter about being removed from the Communist Party.
Cannon described these people as \"petty-bourgeoise-minded people who
can't stand any discipline\", they were also small and very scattered
around the country. Cannon called this the dog days. There would also be
divisions between Cannon and Shachtman and factional disputes, if you
want to know more about this period you can look up The Dog Days of the
Left Opposition by James P Cannon for his perspective on it as a leader
of American Trotskyism. [{% cite alexander1991international %} 764-774]

Minneapolis general strike of 1934
----------------------------------

The dog days came to an end with the General Strike in Minneapolis in
1934, this is one of the most interesting events in American Labor
History and I would highly encourage you to check out Teamster Rebellion
by Farrel Dobs, please pirate it or by it used, don't give the bastards
at pathfinder the money. You can also check out Cannon's account on
Marxists.org \"The Great Minneapolis Strikes\".

But this would be the event that would take American Trotskyism forward,
brought them into contact with workers and actual labor struggles.
However it would have a major downside as it made Trotskyists a major
target by FDR's government.

This is kind of an aside, but the police shot striking workers here and
this launched a major growth in the Teamsters unions in the US and this
event is hardly talked about. Minneapolis was not a friendly place to
unions or organizing and they managed to pull it off, I think any
American socialist needs to read about this event. A short overview I
could do would not do it justice.

Merger with Muste
-----------------

While the Trotskyists were playing a leading role in this strike,
another party the American Workers Party were leading a strike in Toledo
Ohio which like the Minneapolis one resulted in the deaths of strikes
and the calling in of the national guard. These two massive strikes
occurred both in 1934, and it drove both the AWP and the CLA towards
each other. The Trotskyists made a call to the American Workers Party
for them to unify. The AWP were not Trotskyists, but Cannon and the
Trotskyists considered it a \"political menagerie\" that had everything
from \"proletarian revolutionaries to reactionary scoundrels and
fakers\". [{% cite cannon2002history %} 214-215]

This is how James Burnham, and Sidney Hook came into the Trotskyist
movement he was a member of the American Workers Party. It might sound
odd that the Trotskyists merged with a non-trotskyist party, but Trotsky
conceived of the Fourth International being made up of all revolutionary
marxists who agreed with the programme, not just those that called
themselves Bolshevik-Leninists or as they are known now Trotskyists.

French Turn, Entryism
---------------------

Shortly after the merger which formed the Workers Party, the Trotskyists
looked to apply the French Turn also known as Entryism in the US to
further increase their reach. They became more aware that within the
Socialist Party a new left wing was forming especially among the Youth
Sections so Cannon and others started pushing for entry into the SP.

I also want to give a short explanation of what Entryism is, it is
something a lot of people talk about but few know much about it or
Trotsky's conception of it. Especially given many so called
\"Trotskyists\" apply a distorted version of it themselves.

The French Turn was the entry of Bolshevik-Leninists into the SFIO
\"French Section of the Workers' International\"

I could do a video going in depth, but based on points made in Lessons
of SFIO Entriysm and The appeal \"To revolutionary organizations and
groups\"

1\. The goal is not to reform or change a reformist party 2. It is not a
long term tactic, Trotsky described it may be limited to only an
episode. 3. It is to be focused on the youth in an organization which is
often the most radical. 4. It is a hostile tactic the end goal is a
split and the destruction of the reformist organization 5. It is there
to accelerate and assist developing differences, not create them.

So we can see clearly how American Trotskyists applied this to the
socialist party, while it was reformist it had a much more radical youth
wing. So the goal was basically enter and win the youth wing.

In Cannon's words, which is being read by Cyan Lime, another content
creator whos channel you should subscribe to.

\"The Socialist Party was destined, in any case to be torn apart. The
only question was how and along what lines \...

The question was: Would the potentially revolutionary elements of the
centrist party - the worker activists and rebellious youth be engulfed
by these forces? Or, would they be fused with the cadres of Trotskyism
and brought over to the road of proletarian revolution? This could be
tested only by our entry into the Socialist Party. It was not possible
for the Trotskyists to come into contact with these potentially
revolutionary elements of the Socialist Party otherwise than by joining
the Socialist Party\"

Eventually the Trotskyists within the Workers Party talked the party
into joining the Socialist Party, this is how some figures like Sidney
hook ended up in the Socialist Party pulled along with the Trotskyists
when they entered. They would enter in 1936 with the intention of
destroying the Socialist Party their goal was to secure recruits this is
inline with what entryism is a hostile tactic. In this they essentially
would succeed it is a bit more complex, but to keep it short, they would
enter in 1936 and by June of 1937 Trotsky was pushing for a split as he
felt their goals had been accomplished, however he was opposed by Max
Shachtman, James Burnham, and honestly if it was not for the fact they
were purged from the Socialist Party I think Shachtman and Burnham would
have stayed and not ended up in the Socialist Workers Party, but anyway
by April of 1938 the removing of \"Trotskyists\" had been complete. With
the Socialist Workers Party being formed earlier in 38, with much of the
YPSL joining the SWP. The SWP had double the membership the American
Workers Party had entered the Socialist Party with it was considered a
great success, and contributed greatly to the decline of the Socialist
Party. Things looked bright however about as soon as they left the
Socialist Party a major schism would split the SWP.
[{% cite alexander1991international %} 780-799]

The Russian Question
--------------------

Starting even before the split figures like James Burnham and Max
Schatman moved towards and adopted the Bureaucratic collectivism
analysis of the Soviet Union, that the USSR represented not Socialism,
not a Workers State Degenerated or otherwise, or even capitalism, but
that it was a whole new mode of production. This was in contrast to the
position of Trotsky.

### What was Trotsky's analysis of the USSR

Trotsky considered the Soviet Union a Degenerated Workers State, this
mean it was a dictatorship of the proletariat or a workers state that
had undergone a degeneration due to its isolation, that many of these
actions much of which was forced on the state. That this is not because
of the bad intentions of any given man, but historical processes, that
slowly degenerated the leading segments into forming their own caste,
but no class over the proletariat that would inhibit the state from
moving forward to the Lower Phase of Communism or sometimes called
Socialism, instead it was stuck with a capitalistic measure of value,
not a socialist mode of production, but not a fully capitalist state.
The degeneration comes from the part of the state that regulates the
bourgeois norms which exists even under socialism but en even greater
extent while in the transitional epoch, this degeneration would
inevitably continue so as long as the bureaucratic caste was in power.
Trotsky predicted that unless this caste was overthrown the state would
eventually return fully to capitalism. But that an overthrow of this
state would only be a political not social revolution, and from this it
is necessary to advocate for defense of the USSR, but not a defense of
Stalin, but a defense of what progressive elements there is against
Stalin. In The USSR in War Trotsky said if Hitler turned his armies
against the USSR(remember Trotsky didn't live to see the invasion of the
USSR so this was speculation) that the USSR must be defended as they
could not permit Hitler to overthrow Stalin, but that they had to
overthrow Stalin not Hitler at the next stage one Hitler had been
defeated. Trotsky remained against seizures of new territories by the
bureaucracy, and said they cannot take responsibility for Stalin's
actions in Poland or Finland, and that these events showed the need to
rip the USSR from the hands of the bureaucracy.

This is my attempt to summarize Trotsky's position, if your looking for
a more in depth look Revolution Betrayed is a great place to start as is
The USSR in War, thankfully both can be found on Marxists.org

The Split
---------

This all came to a head due to the events of 1939 with the invasion of
Poland and Finland, many become a lot more critical of the USSR and
Trotsky's position on it, some while not outright rejecting the
degenerated workers state theory many felt the USSR had become
imperialist. Trotsky also intensified his already existing dislike of
Burnham and attacked him for his rejection of dialectics and historical
materialism. During a response Burnham would even say Trotsky was just a
stale rehash of Engels, Trotsky however would also invite Shachtman to
Mexico to have a debate, Trotsky was still thinking they could be
debated and convinced, however Shachtman turned him down. This would
climax at the 1940 convention in April where Cannon's group who stuck to
the position of Trotsky, Cannons group got 60 percent of the support
where the minority group got only 40 percent, eventually Cannons group
would try to force the minority group to accept the decisions at this
congress, but began suspending and removing the members from the party.
These people left and formed the Workers Party.
[{% cite alexander1991international %} 802-804]

Workers Party
-------------

The Workers Party would be made up by a few different groups, Max
Shachtman and his Bureaucratic collectivism theory, that still did
advance the idea of conditional defense of the USSR. But you had others
who advanced a State Capitalist position on the USSR, which under that
analysis of it was State Capitalist would mean if it went to war with
German or the US it would be similar to WW1 with competing capitalist
powers at war.

CLR James. Dunayevskaya
-----------------------

The two most prominent members of this group would be C. L. R. James who
is particularly well known for writing a history of the Haitian
revolution titled Black Jacobins, and Raya Dunayevskaya who at this time
wouldn't have been as well known as CLR James who had published several
books like World Revolution and Black Jacobins, she had been Trotsky's
secretary for a while, she would later become well known for her
translations of Marx's early works, as well as her contributions to
Marxist-Humanism and founding the Johnson--Forest Tendency with CLR
James and Grace Lee Boggs, and later the News and Letters Committees.

The Johnson--Forest Tendency would split from the Workers Party because
they supported State Capitalist theory over Bureaucratic
collectivization and due to the workers party lack of interest in black
activism. Because they still believed in the importance of a party they
would end up rejoining the Socialist Workers Party until the 1950s.

If your interested in her positions on this and her disagreement with
the Schamanites on this Marxists.org has her essay \"The Union of Soviet
Socialist Republics is a Capitalist Society.

Shermanites
-----------

Within the Workers Party there was also the Shermanites who were
anti-Bolshevik, the leader of the shermanite faction was Philip Zelznick
who used the name Sherman. This faction considered its self
Revolutionary anti-Bolshevik, and opposed the idea that Marxism should
be the doctrine of the Workers Party. This is the faction that Seymour
Lipset and Irving Kristol were a part of the two neocons we talked about
earlier who were the few to have any kind of contact with Trotskyism and
at most it was being in the Anti-Bolshevik, Anti-Marxist Shermanite
faction of the Workers Party which was founded by members kicked out of
the Socialist Workers Party for their anti-USSR position, as well as for
some their rejection of historical materialism and dialectics. The
Shermanites would leave the Workers Party as well as being kicked out at
the same time as the Workers Party considered them too anti-bolshevik.
So the offshoot of a Trotskyist party who had a lot of issues with
Trotsky and who Trotsky declared members of to not be Trotskyists stil
found this faction of Shermanites which is who the people who would
eventually become neocons too anti-Bolshevik and Marxist and had them
removed after less then a year. [{% cite wald1987new %} 218-225]

Back to Burnham Finally
-----------------------

After that probably too long of a detour back to Burnham, Burnham would
join the Workers Party and be in it for the most brief of moments. After
a few weeks he left the workers party. Acording to Shachtman said that
he refused to use \"trotskyist jargon\" and had only started learning
Trotsky's ideas when the Trotksyists joined the party with the American
Workers Party, which would means he only started picking up these ideas
in 1935-1936. [{% cite alexander1991international %} 803-805]

So Burnham was against the idea of the formation of the Socialist
Workers Party and really only picked up Trotskyism in 1935 at the
earliest, to a year or two being against forming a Trotskyist
Organization, and he rejected Dialectics and Historical Materialism
following this. By the time of WW2 he had pretty much flat out rejected
Marxism, he only ended up in the Trotskyist movement due to them merging
into a party he was a member of. So what at most Burnham could have been
described as a Trotskyist for 3-4 years?

Burnham Conclusion
------------------

So assuming that ok, the idea is that Burnham and the National Review
created neo-conservatism, the typical argument for it is on the basis of
their foreign policy which is not really what defined neo-conservatism,
it was their domestic policy as noted earlier. Also there were plenty of
contributors to the National Review who werent ex-leftists at all, and
there was plenty who were former MLs. The other argument is that the
reason Neocons decends from Trotskyism is the idea that Neo-Cons foreign
policy aims are a global revolution of \"democratic capitalism\", but
Burnham's foreign policy was focused on only working in American
interests he never was in favor of spreading democratic revolution.
[{% cite king-neoconsandtrots %} 264]

So the argument for Burnham to be the proof of the Trot-Neocon pipeline
is weaker if you consider him to be the starting point of American
Neo-Cons then if you went with the other people mentioned above who
contributed to The Public Interest and Commentary.

Inverted Permanent Revolution
=============================

I touched on this already, but a common idea is that Neocons wanted to
spread Democracy by the sword, and Trotsky wanted to spread Communism at
the tip of a bayonet they are the same. However Trotsky his theory The
Permanent Revolution don't argue for this, so this argument really falls
down. I also did a video already on this question in regards to Trotsky
you should go watch it.

Conclusion
==========

So to summarize, a few of the original Neocons briefly flirted with
Trotskyism, before moving onto being Liberals for decades before
founding Neo-Conservatism, and really of them the extent of their
Trotskyism has been greatly greatly exaggerated. As mentioned above the
idea of linking permanent revolution to neo-conservatism does not follow
because permanent revolution does not mean or advocate revolution
worldwide by force or invading, and really Neo-Cons were willing to work
with \"undemocratic\" states as long as it was in the interest of the US
plenty very few of them were these \"crusaders\" for democracy.

I hope you found this video educational, please go watch content from
those other creators mentioned, as always you can find my script in the
description, I am now uploaded them to a website so they are even easier
to view. You should share this video, and subscribe to me on here and
Twitter.

Also sorry for the long break this was not my planned next video, but it
is the one I finished first so I wanted to get it out.

# References

{% bibliography --cited %}
