---
layout: post
author:
- InDefenseOfToucans
bibliography:
- sources.bib
date: February 21 2021
title: "Responding to Sean McMeekin's claim that the Bolshevik-Menshevik split was due to antisemitism"
---

<iframe width= "100%" height= "400px" sandbox="allow-same-origin allow-scripts allow-popups" src="https://watch.breadtube.tv/videos/watch/20de4e91-2534-47b4-a2c3-6004f857b606" frameborder="0" allowfullscreen></iframe>

[This video can also be watched on Youtube](https://youtu.be/fJiEyngxzQM)

[Click here for the PDF version of this script]({{ site.url }}/assets/scripts/assets/scripts/Was_the_Bolshevik_Menshevik_split_actually_because_Lenin_was_antisemitic__2_.pdf)

Introduction
============

So you might be slightly confused with the title as you probably have
not heard this claim before, and I hadn't either until a year or so ago,
a fairly popular Twitter thread claiming this made the rounds and I
asked for their source as it was not a claim I had heard before and they
provided me with one, and well I looked into it and well their evidence
didn't really stack up as I plan to show. I thought this could be an
interesting video to show you can't always trust someone just because
they claim to have a source, and might help people understand how to
verify information, and that we shouldn't just dismiss things out of
hand, too much of the left is too quick to just dismiss things that
conflicts with our understanding of events. So lets take a look at this

The Source
==========

So the source originates from Sean McMeekin an American Historian, in
his book The Russian Revolution: A New History he puts forth the idea
that the split in the Russian Social Democratic Labour Party was due to
Lenin being antisemitic. \"Contrary to the common belief, expounded in
most history books, that the famous Bolshevik-Menshevik split of July
1903 occurred because Lenin's advocacy of a professional cadre of elites
(sometimes called vanguardism), outlined in his 1902 pamphlet What Is To
Be Done?, was opposed by Mensheviks who wanted mass worker participation
in the party, the real fireworks at the Brussels Congress surrounded the
Jewish question. Party organization was not even discussed until the
fourteenth plenary session. Lenin's main goal in Brussels was to defeat
the Bund---that is, Jewish---autonomy inside the party. His winning
argument was that Jews were not really a nation, as they shared neither
a common language nor a common national territory. Martov, the founder
of the Bund, took great umbrage at this, and walked out in protest to
form the new Menshevik (minority) faction. He was followed by nearly all
Jewish Socialists, including, notably, Lev Bronstein (Trotsky), a young
intellectual from Kherson, in southern Ukraine, who had studied at a
German school in cosmopolitan Odessa, which helped prime him for the
appeal of European Marxism. With Lenin all but mirroring the arguments
of Russian anti-Semites, it is not hard to see why Martov, Trotsky, and
other Jews joined the opposition.\"

Now I also take issue with his characterization of the other theory
being that Lenin wanted \"elites\" vs \"Menshevik worker
participation\", but that is not what this video is about I think
ProSocialism did a good set of videos on the topic in both Bolshevik &
Mensheviks: 1903-1904 and In Defence of Lenin's What Is to be Done. I
think the split was about the editorial board composition and how to
define a party member.

So what does it get wrong?
==========================

Just from the start anyone with any familiarity with this congress
should notice a pretty big mistake, The Mensheviks did not walk out with
the Bund, I think maybe he is confusing the 1917 congress where
Mensheviks did walk out, but they did no such this at this congress. On
August 18th 6 Bundists and two Economists walked out from the
congress[{% cite getzler1967martov %} 79] that or maybe he assumed that all Jewish
members of the congress of the bund which is actually probably more
wrong. But I will get back to that, first I should probably explain some
of the background of who exactly the Bund was. Now I wouldn't exactly
call Martov its founder, he was off in the middle of nowhere north of
the arctic circle when it was founded, but his writings had influenced
that partly prompted its creation so in that way I could see why some
people have labeled him as a founder.

What was the Jewish Bund
========================

The bund grew out of a labour movement in Vilnius in Lithuania then part
of the Russian Empire, the city was known for having a large Jewish
population and this got it the nickname of Little Jerusalem. The editors
of two Yiddish newspapers helped found it the Workers' Voice and the
Jewish Worker, the Bund had to hold a congress in secret it was held in
September of 1897 during Rosh Hashanah and Yom Kippur as to avoid
drawing attention of the Okhrana. [{% cite isj_2015] %}

It's foundation was brought about because of the prospects of the
formation of an all-Russian Social Democratic party, see at this time
Social Democrats in Russia were highly split among lots of local parties
all around the country, there had been on on going movement to attempt
to create a national party, the first congress of this was held in March
of 1898 in Minsk. The Congress agreed to the Bund request for autonomy
and that it was the legitimate representative of the Jewish proletariat.
The idea at the time was the Bund would be federated with the Russian
workers organization. However in what shouldn't be a surprise to anyone
with a knowledge of Russian revolutionary movements at the time, but
following the congress there was mass arrests of social democrats in
Kiev, and Moscow and the Bund had its leadership devastated by the
arrests. [{% cite shukman1961a %} I-II]

Iskra
-----

With Lenin and Martov returning from exile and the founding of Iskra,
Martov focused on establishing an Iskra Network in Russia, and in the
mean time the Bund had grown to be one of the main revolutionary groups.
It began to spread into Jewish population centers within the Pale of
Settlement, but this raised issues of how they should organize when
there was already other Social Democratic organizations in those regions
with Jewish members. The fourth congress of the Bund took the position
to not found it where there are other social democrats organized locally
already, they also decided that the bund should be part of a federated
party covering all of Russia with different parties for each
nationality. [{% cite shukman1961a %} iv-vi]

The goal of Lenin and the rest of Iskra was the creation of a national
party given legitimacy by local organizations, and to adopt a unified
programme and for Lenin this meant a German SPD style programme based of
Kautsky's Erfurt programme. This is what Iskra was created to advocate
for. [{% cite leninrediscovered %} 108-115]

Second Congress
---------------

So this is what the second congress was supposed to establish a unified
party with a programme, and to do this they had to answer questions like
the definition of the party member, also what the programme is, and the
parties official newspaper and editorial board, which was to be Iskra.
It also had to answer questions of how to handle the relationship with
organizations like the bund. The Iskraists wanted a unified party, not a
federated party.

Did Martov and Trotsky leave due Lenin's antisemtism?
=====================================================

Alright now you know some of the background information, now let us get
into the meat of the question. Well we already know that Martov and
Trotsky didn't walk out as I established earlier.

But to start with let us take a look at Mr McMeekin's source. Which is
Harold Shukmans 1961 PhD thesis \"The relations between the Jewish Bund
and the RSDRP 1897-1903. and his citation is for page 237. Now
thankfully and I would like to say how much i appreciate this being free
to download as a PDF from the Oxford Archives website.

So looking at his source, it seems to not put forth the same argument or
really any kind of evidence of what he claims his source offers none. In
fact it argues the opposite on page 242 it even points out and I quote
\"Martov the manager of the Congress, the chief spokesman for Isrka, the
Bund's worst enemy.\" This even touches on something I will come back
to, but his source mentions the Bund didn't support Martov because
Martov was pro-bund, but they figured they could widen the split in
Iskra which would give them more room to argue for their position they
felt.

So his own source disagrees, it says Martov was an enemy of the Bund, it
makes no mention of Trotsky or Martov really being angry or upset about
how Lenin spoke of the Jewish people. Well he has no real source for
this, but lets investigate someone can still be correct with no source.

So what about Trotsky
---------------------

Well maybe a place to start fpr trying to verify this claim is Trotsky's
My Life his autobiography

Well he says 

>\"How did I come to be with the "softs" at the congress? Of
the Iskra editors, my closest connections were with Martov, Zasulitch
and Axelrod. Their influence over me was unquestionable. Before the
congress there were various shades of opinion on the editorial board,
but no sharp differences. I stood farthest from Plekhanov, who, after
the first really trivial encounters, had taken an intense dislike to me.
Lenin's attitude toward me was unexceptionally kind. But now it was he
who, in my eyes, was attacking the editorial board, a body which was, in
my opinion, a single unit, and which bore the exciting name of Iskra.
The idea of a split within the board seemed nothing short of
sacrilegious to me.\"

He seems to argue his reasoning was mostly out of personal reasons
connected to the editorial board, as he was closest to Martov, Zasulich,
and Axelrol. Lenin argued and got passed at the congress a change to the
editorial board to be made up of Plekhanov, Martov and Lenin. Trotsky
also says later he also failed to fully realize how much centralism was
going to be required of the party.

So no mention of anti-semitism. But, this was written in 1930 after
Trotsky in 1917 had joined the Bolsheviks, and was in an argument within
the Third International about the correct path,so he would have some
motivation to not want to insult Lenin.

Hmm, but the good news is we have Trotsky's response to the split when
he was mad at Lenin, so surely if he had actually joined the Mensheviks
out of Lenin being antisemitic he would have mentioned in that polemic
he would have had no reason not to.

This work is 'Our Political Tasks' written as a response to Lenin about
the split, and in part What is To be Done?.

and well, not a single mention of Jews, or the Bund.

Other Responses
---------------

Both Kautsky and Luxemburg wrote response on the Russian split and
neither of them made any mention of it actually being because Lenin was
antisemitic, so if this split was actually because of that no one told
the people involved.

Lenin, Trotsky, and Martov on the Bund
--------------------------------------

So lets talk about the actual positions of Lenin, Trotsky and Martov on
the Bund because they actually really didn't disagree on this.

### Martov

In August of 1901 Martov launched Isrka's first main assault on the
Bund, he attacked it for developing Jewish Nationalism which he
considered a major political mistake, as well he attacked them for the
demand for Jewish autonomy, he considered the main issue in the Russian
empire to be that Jews were separated by the policy of the tsarists
government. He said that the Bund's nationalism and demand for
federalism weakened the organizational ties between Jewish and Russian
workers. The goal is not territory for Jews, but to be free to spread
and settle all over Russia. The bunds function in his view was it should
engage in special agitation among Jewish workers on the basis of their
oppression, but not take on nation tasks. [{% cite getzler1967martov %} 56]

Martov would also release two lengthy articles in March and June of 1903
attacking the bund, he argued similar to above that Jewish Socialists
needed to join an organization of the whole of the Russian proletariat,
and that the nationalism within the bund had to be fought. That their
idea of an independent national party separated the Jewish workers from
Polish and Russian Workers. [{% cite getzler1967martov %} 58-59]

Now actually at the congress as Harold Shukmans wrote, Martov was the
main enemy of the Bund, Israel Getzler in his biography says something
very similar

>\"Martov was the main speaker for the coalition against the Bund at the
Second Congress and moved the resolution which rejected Bundist
federalism and in effect drove the Bund out of the party\"

### Trotsky

So we talked about earlier that Trotsky was not bothered by Lenin's
position. Trotsky was much in the same position as Martov, as is noted
by Isaac Deutscher, and I think very clear from the minutes of the
congress, it was very much intentional on the part of Lenin and the
Iskra supporters to have Martov and Trotsky lead the attack on the Bund,
they wanted the attacks on the Bund to come from Jewish members of the
congress. When Martov brought up his motion against the Bund only Jewish
delegates signed it, Trotsky in his attacks on the Bund was accused by
the Bund of offending Jews. Trotsky was in the same opinion as Martov,
Axelrod and other Jewish Socialists, in opposition to the idea of a
separate Jewish Nation, Trotsky said while the Bund was opposed to
Zionism they had absorbed the nationalism from them. Trotsky would
repeat this attack in articles after the congress attacking Zionism.
[{% cite deutscher2003prophetarmed %} 72-75]

### Lenin

So while we know Lenin hung back what exactly were his positions on the
Bund. Well they were not exactly any different from Isrka on the whole
other then maybe he was more willing to see them leave then Martov was.
But in the end Martov's resolution was the one that drove the Bund out
of the party. A another position Lenin took in opposition to the Bund
was Lenin's position that the Jewish people ceased to be a nation as
they had no territory or single language, but this was not an exclusive
position to Lenin, this was the position of Karl Kautsky who Lenin
referenced in some of his writings on the subject. This was the same
that Martov and Trotsky took as well, so I am not quite sure how they
could have been bothered by it and how it could have been the source of
the split.[{% cite krausz2015reconstructing %} 257-260]

Now a question I have avoided is what Lenin said actually antisemitic,
and I don't think that actually matters within the context of is the
historians claims correct or not, because the question is if it caused
the split and Trotsky, Martov certainly didn't think it was antisemitic
and it is certainly not why they were part of the Mensheviks, neither
Luxemburg or Kautsky seemed to have thought it was an issue either.

Reviews from Other Historians
=============================

When I saw someone citing this book I remembered I had heard about it
before from a historian I have quite a lot of respect for Sheila
Fitzpatrick, in an article titled \"What's Left?\"

>\"McMeekin, the youngest of the authors here, set out to write a 'new
history', by which he means an anti-Marxist one. Following Pipes, but
with his own twist, he includes an extensive bibliography of works
'cited or profitably consulted' that omits all social histories except
Figes. This includes Smith's and Steinberg's earlier books, as well as
my own Russian Revolution (though it is cited on p.xii as an example of
Marxist, Soviet-influenced work). It could be argued that McMeekin
doesn't need to read the social histories since his focus in The Russian
Revolution, as in his earlier work, is on the political, diplomatic,
military and international economic aspects. He draws on a multinational
archival source base, and the book is quite interesting in detail,
particularly the economic parts. But there's a whiff of right-wing
nuttiness in his idea that 'Marxist-style maximalist socialism' is a
real current threat in Western capitalist countries. He doesn't quite
call the whole revolution, from Lenin's sealed train in April 1917 to
the Rapallo Treaty in 1922, a German conspiracy, but that's more or less
what his narrative suggests.\"

This can be a good way to help identify possible issues with sources is
to look up reviews from historians. Though unfortunately some other
reviews of this work I tried to find are behind paywalls and I couldn't
get access to them.

Conclusion
==========

So really to conclude regardless of if anyone listening to this
considers Lenin's position to constitute antisemitism I see no evidence
for the claim that this somehow was the driving reason for the split
given the broad agreement between Lenin, Martov and Trotsky on this. It
was an issue for the Bund, but not the Mensheviks. Now I also want to
say something I talked about this a bit on Twitter and someone pointed
out that they had Sean McMeekin as a teacher and he was a nice guy. So
don't mean this to be an attack on him, though I do think his position
in his work is wrong on this. This is also only a comment on this
section of his work, not it on the whole I have not read much of
anything else put out by him so I can't comment on it.

Anyway hope you enjoyed this shorter video please check out my other
videos and subscribe, if you want to check my sources for this the
script is on my website, which I have been considering doing a video
about how it is setup let me know if you would find that interesting,
otherwise my next video is doing to deal with the butchering of animals
during the 30s famine, and a review of a WW2 Red Army Reproduction
ration.


{% bibliography --cited %}
