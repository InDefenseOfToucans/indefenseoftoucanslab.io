---
layout: post
author:
- InDefenseOfToucans
bibliography:
- sources.bib
date: January 28 2020
title: 'Was Trotsky for Spreading the Revolution via the Red Army?'
---


<iframe width= "100%" height= "400px" sandbox="allow-same-origin allow-scripts allow-popups" src="https://watch.breadtube.tv/videos/embed/ffd4bcbf-fc93-446b-b8cd-9d8f42715bf0" frameborder="0" allowfullscreen></iframe>

[Click here for the PDF version of this script]({{ site.url }}/assets/scripts/trotskyspreadingrevolutionviaredarmy.pdf)


Introduction
============

I had a someone request a video on this topic, so I figured this could
be a short little debunking video. People continue to think Leon
Trotsky's Permanent Revolution advocates for Military Adventurism as in
for invading other countries. I think this is a product of people
getting Trotsky's role as the leader of the Red Army, mixed in with the
incorrect narrative that the 1920s debates in the USSR being between
Stalin's Socialism in One Country Vs Trotsky's Permanent Revolution. I
some day hope to do a series of videos on what the arguments actually
were about in the 1920s, but for now I wanted to make a quick video
debunking the idea that Trotsky advocated for growing the revolution by
Red Army as I need a break from the project I have been hammering away
at for 2 months now.

No Trotsky was not for Military Adventurism
===========================================

In the 1930s Trotsky was interviewed by Albert Goldman an American Civil
Rights Lawyer as part of the Dewey Commission \"GOLDMAN: But assuming
there is no dual power in a country. Assuming that the proletariat does
not attempt to take power. Did you ever believe or advocate the idea
that the Red Army should be sent into other countries?

TROTSKY: A revolution by the Red Army would be the worst adventurism. To
try to impose revolution on other people by the Red Army would be
adventurism.\"

But a quote from 1937 is not enough, we should have more evidence then
just Trotsky's words on his position in 37.

So let us look at a few historical examples.

Brest-Litovsk
=============

In December of 1917 in a speech Trotsky did say that a revolutionary war
might be needed, but this was on the condition the Kasier refused peace
and even then his his speech he debated himself over if it would even be
possible.

\"If this dead silence were to reign in Europe much longer, if this
silence were to give the Kaiser the chance to attack us and to dictate
terms insulating to the revolutionary dignity of our country then I do
not know whether - with this disrupted economy and universal chaos
entailed by war and internal convulsions - weather we could go on
fighting\" Trotsky then started no that they could build a powerful army
of soldiers and Red Guard and fight if they had to. [{% cite debo1979revolution %}
47]

The Bolsheviks had initially hoped that there might be peace without
annexations, that regions might be given the right to
self-determination. This can be seen in my first Brest-Litovsk video, I
have not covered yet but eventually the Germans would offer no such
terms. In January This time Trotsky advanced the idea of termination of
the war without signing any peace and demobilize. He felt Germany would
be too weak to actually attack them, of course this would end up being
wrong, but this was the position Trotsky took, he did not advocate
revolutionary war. [{% cite debo1979revolution %} 69] However a faction of people
did, I will cover this more in depth in the future in the next video on
Brest-Litovsk which should hopefully come out in the next months.
Bukharin was the main leader of those who were opposed to the treaty and
for revolutionary war.

So we can see in the treaty of Brest-Litovsk while Trotsky felt it might
be an option it was in reaction to the idea of the Kaiser refusing peace
and continuing to invade, not spreading the revolution at the end of a
bayonet.

Poland
======

Let us take a look at Trotsky's position on the war with Poland, worth
mentioning that the war between the Soviet Republic and Poland was
sparked by border skirmishes that had broken out without orders from The
Polish or Soviet governments that occurred due to Poland seizing parts
of Belarus and Ukraine. Like many events I want to dive further into
this topic in a future video.

In July of 1920 Trotsky stated his intention with all Ukranian and
Belarusian territory secured he would order the red army to halt and not
advance any further and to make a public offer of peace. Lenin and the
majority of the Politbureau were for the continuation of the war into
Poland. No one in this fight argued for the idea that Communism and
Revolution be forced on an unwilling Polish population. Lenin and others
knew in 1917 there had been Soviets in Poland and very strong support
for Communism there, they believed them to still be there and strong.
Even in early 1920 Trotsky spoke about Polish Soviets, none of them were
fully aware to what extent they had been suppressed in Poland in the
years sense. The Politburo asked Polish Communists who had joined the
Bolsheviks and lived in Russia their opinions. Karl Radek, and Felix
Dzerzhinsky opposed the invasion and said it would result in a surge in
Polish patriotic sentiment. Another Polish Communist Lapinsky greatly
exaggerated the strength of Polish Communism. Trotksy would side with
the opinions of Polish Communists who opposed it. Trotsky would submit
to the decision of the majority and carry out his job despite opposing
it. When this war turned to disaster Trotsky argued in favor of a peace
deal, which Lenin would support him on. [{% cite deutscher2003prophetarmed %}
464-469]

Georgia
=======

On to Georgia, now this one is a bit more complicated then the others,
this is yet another topic I would love to dedicate several videos to, so
can look forward to that in the future.

But we can cover enough to talk about Trotsky's opinion of it.

In 1920 the Russian Soviet Republic concluded a treaty with Menshevik
Georgia recognizing its Independence. However in 1921 the Red Army
invaded and seized the country. Trotsky who was in the Urals at the time
of the invasion was enraged and when he returned to Moscow to he
demanded the creation of a commission to investigate the events
[{% cite smithgeorgianaffair]and %} \"bring to book the presumed adventurer\"
[{% cite deutscher2003prophetarmed %} 474] he would lose the vote, however.

Trotsky in 1922 wrote Between Red and White and in it defend the
invasion to some extent though he was ordered to write the book by the
Politburo which might explain its difference with his earlier actions on
the matter. Later in his Biography of Stalin, Trotsky would return to
being critical of the invasion as he had been at the time.

Conclusion
==========

To conclude, Trotsky in many situations during the civil war era opposed
invasions even in the case of Poland where the Bolsheviks though there
would be support among the workers for it. Now I don't want to go into
Trotsky and the Left Oppositions actual opinions as that is a topic for
a future video, but during those debates Trotsky was not calling for
invading other countries to export the revolution.

In all was kind of fun doing a video by request, feel free to ask me in
the comments or on Twitter if you want a topic or a point debunked. My
channel's overall goal is not to just debunk individual points but
provide a more accessible way to learn about Soviet History. But I kind
of liked doing this one. My next video after this should be one on Yakov
Sverdlov, I might also produce a video going over my goals for my
channel for 2020 and my overall plans for content in the near future.

# Refrences

{% bibliography --cited %}