---
layout: post
author:
- InDefenseOfToucans
bibliography:
- sources.bib
date: July 17 2020
title: Bukharin Vs Lenin on the Theory of the State
---

<iframe width= "100%" height= "400px" sandbox="allow-same-origin allow-scripts allow-popups" src="https://watch.breadtube.tv/videos/embed/56dc7ac1-37ff-4769-aae8-903f5f4ed71e" frameborder="0" allowfullscreen></iframe>

[Click here for the PDF version of this script]({{ site.url }}/assets/scripts/BukharinVsLeninontheTheoryoftheState.pdf)



Introduction
============

A now often forgotten aspect of Lenin State and Revolution is that Lenin
did not always hold those views, and they were actually heavily
influenced by Bukharin. This fact would have been indisputable before
about 1930, but now it is nearly fully forgotten by many people who
claim to follow Lenin's ideas. This I think it shows us that Lenin too
made mistakes and changed his views and helps us understand those
evolution in his thinking. Originally this was just going to be a
portion of the biography video on Bukharin I have been working on, but
that is pushing 2 hours long so I thought this might work better as a
stand alone video. Still going to make the biography video, but I wanted
to get this out now.

Bukharin and Lenin Fight
========================

In early 1916 Bukharin produced \"Towards a Theory of the Imperialist
State\" sending it to Lenin to be published. Bukharin would take what
were some controversial positions especially at the time, Lenin was also
expecting an economic article from him, not a article on the theory of
the state.

Here is a few paragraphs from it which Lenin disagreed with greatly.

>\"Thus, the society of the future is a society without a state
organization. Despite what many people say, the difference between
Marxists and anarchists is not that the Marxists are statists whereas
the anarchists are anti-statists. The real difference in views of the
future structure is that the socialists see a social economy resulting
from the tendencies of concentration and centralization, the inevitable
companions of development of the productive forces, whereas the economic
utopia of the decentralist-anarchists carries us back to pre-capitalist
forms. The socialists expect the economy to become centralized and
technologically perfected; the anarchists would make any economic
progress whatever impossible. The form of state power is retained only
in the transitional moment of the dictatorship of the proletariat, a
form of class domination in which the ruling class is the proletariat.
With the disappearance of the proletarian dictatorship, the final form
of the state's existence disappears as well.\"

>\"In the growing revolutionary struggle, the proletariat destroys the
state organization of the bourgeoisie, takes over its material
framework, and creates its own temporary organization of state power.
Having beaten back every counterattack of the reaction and cleared the
way for the free development of socialist humanity, the proletariat, in
the final analysis, abolishes its own dictatorship as well\"

Lenin initially considered publishing it as a discussion article, but
found it too incorrect on the question of the state. Now without fully
going into the history of this, Kautsky and many prominent other
\"Marxists\" of this era had completely dropped any idea of the smashing
of the bourgeois state and the construction of a new workers state or a
dictatorship of the proletariat, this was a product of them capitulating
and becoming reformists.

So Bukharin was going against the most common Marxist understanding the
state and revolution at the time.

Lenin felt that Bukharin had taken Engels out of context, and
specifically his idea that Anarchists and Marxists do no differ on the
state as being very incorrect. Lenin even went as far to accuse him of
semi-anarchism, and said he fully downplayed the need for a state post
revolution. [{% cite cohen1973bukharin %} 39-40] I think this was a unfounded
accusation as Bukharin wrote, \"the proletariat destroys the state
organization of the bourgeoisie, takes over its material framework, and
creates its own temporary organization of state power\"

This debate would be carried out in letters, which at least according to
what I can find have never been published in full. So we can't really
take a look at those unfortunately. [{% cite cohen1973bukharin %} 398] This debate
would act as a sort of catalyst for Lenin to take a deeper look at the
state in the works of Marx and Engels, the major impact Bukharin would
have on Lenin was an accepted fact in the Soviet Union even acknowledged
by Stalin in 29, however in the years following that there was attempts
to downplay Bukharin's role. [{% cite cohen1973bukharin %} 399]

During this debate Bukharin mood was impacted and he fell into lower
spirits, Bukharin made the choices to take a steamer to America, this
was at the recommendation of a friend and fellow Bolshevik Alexander
Shlyapnikov. Shlypanikov told him he should go write for a newspaper
edied by Alexandra Kollontai in New York[{% cite allen2016alexander %} 72] When
Lenin heard about this became deeply worried that he had driven Bukharin
away, he inquired someone to find out with what mood is Bukharin leaving
and will he continue to write to Lenin and others, and still fill
requests for other documents. Lenin would then receive Bukharins
farewell letter. I am going to quote in full the closing plea that Cohen
includes from Bukharin's letter.

>\"I ask one thing of you: if you must polemicize, ect., preserve such a
tone that it will not lead to a split. It would be very painful for me,
painful beyond endurance, if joint work, even in the future, should
become impossible. I have the greatest respect for you; I look upon you
as my revolutionary teacher and love you.\"

Lenin would respond saying the charges were valid, but he gave Bukharin
praise and said \"We all value you highly.\" Bukarhin would respond \"Be
well, think kindly of me \.... I embrace you all,\"

Bukharin Wins and Lenin shifts his opinion 
==========================================

In December of 1916 Lenin wold say he was working towards producing an
article of his own on it. To do this Lenin started gathering all the
works of Marx and Engels he could and rereading it all. Lenin wrote and
prepared a notebook from January and February, these notes would form
the basis of what would become The State and Revolution. Lenin after
examining the question concluded that Kautsky was far more wrong then
Bukharin, but he still felt that Bukharin was wrong about a few things.
Then in May Krupskaya told Bukharin that Lenin no longer had any
differences with him on the question of the state. According to Cohen it
is possible in a letter or through someone else Bukharin might have been
made aware of Lenin's shift in opinion earlier.

In July of 1917 Lenin had to go into hiding and told Kamenev if the
provisional government was able to kill him that Kamanev was to publish
the notes. Lenin managed to evade capture and
tuhttps://www.reddit.com/r/BreadTube/new/rned his notes into The State
and Revolution while hiding in Finland. It was originally planned to be
published in 1917, but the revolution put a hold and it was not to be
published until January 1918.

We can see Lenin came to Bukharin's positions he previously was critical
of by just looking at The State and Revolution. Let us took on the
question of the abolition or smashing of the bourgeois state.

>\"Engels speaks here of the proletariat revolution "abolishing" the
bourgeois state, while the words about the state withering away refer to
the remnants of the proletarian state after the socialist revolution.
According to Engels, the bourgeois state does not "wither away", but is
"abolished" by the proletariat in the course of the revolution. What
withers away after this revolution is the proletarian state or
semi-state.\"

>\"From 1852 to 1891, or for 40 years, Marx and Engels taught the
proletariat that it must smash the state machine.\"

>\"This, in turn, results in state organization of the bourgeoisie, takes
over its material framework, and creates its own temporary organization
of state power.\"

Of course Lenin majorly expanded on the writings on the Dictatorship of
the Proletariat, as well as includes major quotation from Engels and
Marx and a more polemic against Kautsky.

Now how about Bukahrin's position of needing to emphasis the inherent
opposition to the state, as well as that Anarchists and Marxists don't
disagree on the question of the state under communism.

>\"To prevent the true meaning of his struggle against anarchism from
being distorted, Marx expressly emphasized the \"revolutionary and
transient form\" of the state which the proletariat needs. The
proletariat needs the state only temporarily. We do not after all differ
with the anarchists on the question of the abolition of the state as the
aim.\"

Lenin would agree that there is no difference with Anarchists on the
eventual aim to have no state, but that its a question of the time
scale, and the need for the proletariat to construct its own state which
would be abolished in the form of it withering away. As well as maybe
the most famous quote from The State and Revolution \"So long as the
state exists there is no freedom. When there is freedom, there will be
no state.\" So just as Bukharin did have here Lenin emphasising the
opposition to the state. ng of revolutionary Marxists, that is to say,
communists, and that of social-opportunist traitors who have turned
their backs on the teaching of Marxism and who swear by the name of Marx
but at the same time betray his teaching in the most cynical way.

Lenin's little book perfectly shows this difference. And the reader
cannot blame the author for extensively quoting the works of Marx and
Engels. These works silence these vile slaves of capital who say they
are social democrats, as they silence all the Mensheviks, the SRs, the
Bundists, the followers of Scheidemann and Novaya Zhizn (New Life) who
dare to speak on behalf of the great masters.

Today every comrade has to read Lenin's book.\"

Impact in 1917
==============

I explore this more in the biography video, the position of smashing of
the bourgeois state was key in Bukharin having the correct position of
socialist revolution and the smashing of the bourgeois state being the
provisional government in Russia in 1917. It clearly can be seen in
Lenins position once he returns to Russia calling for revolution and a
DoTP or commune state. Where many other Bolsheviks lagged behind and
thought the provisional government must be supported and participated
in. These new views of Lenin actually got him described as an anarchist
by Mensheivks and Bolsheviks

Conclusion
==========

Hope this was helpful, I would highly recommend you read the works
mentioned, both Lenin's State and Revolution and Bukharin's though
Bukharin's original article apparently is not in its complete form and
has been lost to time. I think all of this is important to know as I
think it aids in understanding State and Revolution and the context
behind it. I think it is a shame due to Bukharin's fall from power in
1930 and execution in 1938 this contribution was intentionally erased
and then forgotten. You should also give Bukharin's The Economics of the
Transition Period, and what he wrote alongside Preobrazhensky The ABC of
Communism. Both are good works that touch on this same subject.

If you want to know more about Bukharin you can wait for my video to
come out which is hopefully soon, but you can also pick up Stephen F.
Cohen's excellent biography of him which also was a major source I used
for this video.

# References

{% bibliography --cited %}