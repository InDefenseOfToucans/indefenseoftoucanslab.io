---
layout: post
author:
- InDefenseOfToucans
bibliography:
- sources.bib
date: April 2 2021
title: 'Nikolai Ivanovich Bukharin'
---

Introduction
============

I am back with another biography covering another Bolsheviks, this time
I am covering Nikolai Bukharin, he is an incredibly important figure in
the history of the Bolsheviks and really communism as a whole. Lenin
would say of him \"Bukharin is not only a most valuable and major
theorist of the Party; he is also rightly considered the favourite of
the whole Party\", this despite many of their historic disagreements.
Bukharin would be one of the main figures in party leadership in 1917
and remain a major figure until 1929, retaining some influence until
1938 when he was ordered to be executed that was authorized personally
by Stalin. Despite all this I do think he tends to be a lesser known
figure within the Bolshevik party.

Birth and Early Life
====================

Nikolai Ivanovich Bukharin was born October 9th 1888, both of his
parents were primary school teachers in Moscow. His father was a
graduate of the Moscow University and was a school teacher until 1893
when he became a tax collector, four years later he would lose this post
and would be unemployed for a few years following leaving the family
rather poor. Both of Bukharin's parents being educated and members of
the intelligentsia mean Bukharin was raised very well educated for the
time. He would finish primary school as one of the best students, and
would enroll in one of the best gymnasiums in Moscow. He would do
exceedingly well there without exceeding much of any effort into
classes. [{% cite cohen1973bukharin %} 6-10]

From a young age he was quite an adventurous and curious kid, he took a
very strong interest various creatures from tarantulas to birds, he even
built a small sorta zoo.[{% cite bukharin1998all %} 37-43] This interest never
went away especially with birds, which is honestly why I like Bukharin,
this may come as a bit of a surprise but I like birds I think they are
interesting, and Bukharin liked birds too.

If you want to know a bit more about Bukharin's early life his memiors
he produced while in prison

Becoming a revolutionary
========================

Bukharin now 16 in the higher grades of the gymnasium, it was the year
1905 in Russia and he was swept up in the growing radicalism. He would
be drawn to the center of much of the revolutionary activity in Moscow
the State University, in lecture halls students, workers and
revolutionaries would listen and make speeches, it was here Bukharin
would be drawn to Bolshevism as well as a lot of fellow revolutionaries
from Moscow, this would have a profound impact on the Bolshevik party,
this brought in many now young but future leaders in the party including
Bukharin. There would be a group of Muscovites of which Bukharin would
be the most famous member of formed a group of Friends around Bukharin
many would be his allies in various party struggles. [{% cite cohen1973bukharin %}
9-10] This also shows an earlier example of one of Bukharin's major
qualities he was known to be friendly and good humor, as well apparently
known to be quite attractive to women [{% cite cohen1973bukharin %} 13]

He was also likely pulled to the Bolsheviks in part due to their
domination in Moscow it was one of the few locations within the Russian
Empire at the time where the Bolsheviks held majorities over the local
party committees. Important to know that at this time the Bolsheviks and
Mensheviks were both part of the Russian Social Democratic Labour Party,
so a lot of party committees around the country were either controlled
by either group, or sometimes groups of people who were more in the
middle, this would remain the case until the 1910s when they more
properly separated. Also a note on the term Social Democrats, in this
context the Bolsheviks, Mensheviks would both be considered Social
Democrats within Russia at this time, it would be some time before
social democracy would pick up its modern definition, anyway back to
Bukharin.

A Bolshevik
===========

After the year of 1905 had passed the meetings at the University
stopped, and so did the demonstrations and barricades. Bukharin in1 1906
would join the Bolsheviks, his job within the party was to act as a
propagandist, he would along with Grigori Sokolnikov who would in the
future be the Commissar for Finance and a member of the Bolshevik
central committee in 1917. Sokolnikov and Bukharin would unify the
Moscow youth groups and would hold a national Congress of social
democratic student groups in 1907. The congress of student organizations
would approve of the programme and tactics of the Bolsheviks, this
congress of student organizations would soon be destroyed due to police
harassment. Not all of his work at this time was student, he would
participate in industrial organizing and lead a strike of workers at a
wallpaper factory. Bukharin would also in 1907 be enrolled in economics
at Moscow University, though with all of his party work he hardly if
ever was in class. This did not mean he had no interest in economics it
in fact would become Bukharin's primary interest later on. During this
time he was part of organizing Marxist schools, student demonstrations,
including \"raids\" where a number of Bolsheviks would go to lecture or
presentation from a liberal professor to attack and critique them. He
would quickly rise through the ranks and by 1908 was part of the Moscow
part central organization and it would be ratified in 1909, this would
make Bukharin a high ranking Bolshevik in the largest city. Though this
would mean he quickly caught the attention of the Tsarist secret police
the Okhrana and he would be arrested in May of 1909, though released
soon after then arrested again and released again under security pending
trial. During this time separate factions or groupings were forming
within the Bolsheviks, i talked about this briefly in my video on
Lunacharsky, but a group around Bogdanov was forming that was opposed to
Bolshevik participation in the Duma, where Lenin was for it. As well as
Bogdanov started supporting other philosophies and tried to merge then
with Marixsm. Lenin was deeply opposed to both positions, Lenin
supported Bolshevik participation in the Duma, and Lenin wrote
Materialism and Empiriocriticism. Bukharin while he agreed with Lenin
and did not agree with Bogdanov politically he admired Bodanov and his
works.

During this underground work became very difficult, Bukharin moved to
doing more legal work during this time, he did eventually go into hiding
in 1910 though due to secret informants in the party all of the Moscow
leaders were rounded up. Bukharin's hiding spot was not known to many
people, he began to suspect Roman Malinovskii a fellow high ranking
Bolshevik in Moscow, this would become more of an issue later but we
will talk about this later. When arrested Bukharin was imprisoned in
Moscow for 6 months he was then exiled to Onega in June, Bukharin feared
he would soon be moved to a penal colony and he disappeared from Onega
August 30and left Russia. Fleeing the country was a pretty normal thing
for many of the revolutionaries in Russia at this time.
[{% cite cohen1973bukharin %} 10-15]

Outside Russia
==============

Bukharin would go to Hanover Germany, skipping meeting with Lenin
something that was typical of Bolsheviks when exiled. Bukharin would
eventually go to meet Lenin in September of 1912 he would meet Lenin for
his first time. Their long conversation became focused around Roman
Malinovskii a person Bukharin suspected of being a police agent, during
this time Malinovskii had become a leading Bolshevik he was on the
central committee and head of the Bolsheviks in the Duma. Bukharin was
not alone in suspecting of being a police agent many other Bolsheviks
and Mensheviks suspected him of being a police agent, but Lenin would
not hear any of it and it became a major source of friction between
them, Lenin in an argument with Bukharin would accuse him of being a
gossip in 1916 due to proposing the idea. Though in 1917 with the
opening of the police records it was revealed to be true Malinovskii was
an agent for the Okhrana. [{% cite cohen1973bukharin %} 13,18] Lenin would
actually be put on a trial of sorts by the Extraordinary Investigatory
Commission of the provisional government and he would be questioned
about why he refused to believe he could be a police agent. He expressed
that the evidence was scant and unconvincing, and that Lenin was
impressed by his trade union work. [{% cite 10.2307/40870092] %}

Bukharin would be invited to contribute to party papers and Lenin was
quite happy with his work despite the friction over Malinovskii.
Bukharin's reason for moving to Vienna was to begin a project to
criticize the work of bourgeois economists and a defense of Orthodox
Marxism. This work \"The Economic Theory of the Leisure Class\"
published in 1914 would be Bukharin's first book, the works criticisms
of some bourgeois economics took a lot from the Marxist Rudolf
Hilferding, and this was not the last work that would take influence
from Hilferding, both Lenin and Bukharin's writings on Imperialism would
be highly influenced by Hilferding.

This was not the only thing Bukharin worked on during this time, in
January of 1913 Bukharin would aid a Georgian Bolshevik sent to Vienna,
Stalin at this time needed assistance not in the theory, but in
translations as Stalin did not know German. [{% cite cohen1973bukharin %} 21]
After this Stalin would return to Russia and be arrested I only mention
this due to the fact Stalin's location was betrayed by Roman Malinovsky.
[{% cite tucker1974stalin %} 155-157] Also just to finish this point really want
to reinforce that Bukharin only aided in the form of translation and
possibly some other minor work. Stalin was the author of the work,
Robert C. Tucker in his book Stalin as Revolutionary explains the
reasons why theories as to other authors or the idea of anyone else
writing it with Stalin is wrong.

Anyway back to Bukharin, with the start of World War 1 Bukharin would be
deported and end up in Switzerland, while there he with a few other
Bolsheviks they decided to publish a new newspaper, Lenin would became
aware of this in January of 1915 and would become quite angry with them,
accusing them of starting an opposition newspaper. Bukharin explained
that their paper was not an opposition but a supplement, but Lenin would
not hear it.

As well in February and March there was a party conference held in Bern,
here Bukharin would find himself in disagreement with Lenin again on 4
points with regard to the party and the war.

Point 1, he opposed the appeals to the petty bourgeoisie this included
peasants and opposed the idea of them as a revolutionary force or an
ally.

Point 2, He wanted more emphases on socialist demands.

Point 3, While they agreed with Lenin on turning the imperialist war
into a civil war, they wanted more appeals to the general anti-war
movement and they did not think Russia's defeat would be a lesser evil
but felt all belligerents should be condemned.

Point 4, Lenin's call for a new international they felt it should
include all anti-war social democrats, and left wing mensheviks built
around Leon Trotsky.

There would be other aspects where Bukharin would argue on the same side
as Lenin, and a commission meant to work out the differences made up of
Lenin, Zinoviev and Bukharin, after some arguments the final resolution
was passed unanimously. [{% cite cohen1973bukharin %} 22-24]

So while there were disagreement Bukharin still found himself very much
in broad agreement with Lenin, the arguments and disagreements I think
are in part because as I am sure we have all experienced it is sometimes
the most frustrating to argue with someone rather close to your own
opinions, but not quite. I think Lenin very much agreed with Bukharin on
most things and that made some of these smaller disagreements all the
more frustrating to him.

In 1915 Bukharin would also produce his work on Imperialism that would
have a large impact on Lenin, though his version would have some
differences from Lenin. To talk about them in depth is really out of
scope of this video, Lenin read Bukharin's work and wrote an
introduction for it, so what differences they are minor. If you want to
get a feeling for the differences you really should read both.

Bukharin was also preparing a work on the Marxist theory of the state,
this would result in an argument with Lenin, but this occurred a bit
later so we will come back to it.

Arguments on Self-Determination
===============================

Lenin over the course of the 1910s put more and pure of a focus on
colonialism and problems of self-determination, not the say it was not
already a debate within the party, the fight against the Jewish Bund by
Lenin and Martov already contained some arguments on this matter, and
the party congress of 1903 included provisions on self-determination,
and local self rule. Lenin had asked Stalin to prepare a response
against the Austrian Marxists. Where Lenin wrote The Right of Nations to
Self-Determination to argue against primarily Rosa Luxemburg, who's
positions on the national question were quite popular amongst
Bolsheviks. [{% cite smith1999bolsheviks] %}

I think it is worth mentioning too that some within the Bolsheviks who
were sympathetic to Luxemburgs positions actually went way further then
her in opposing it, especially when it came to in practice, Piatakov was
a Russian Chauvinist as shown by his time in charge of Ukraine. Rosa
still supported some local governance and rights of language and
culture. Karl Radek and others supported Luxemburg's position, and
Bukharin came to as well. It was also not that Rosa and some of the
advocates of her position felt that what occurred to small nations
didn't matter but felt language rights local self-government and
equality before the law were sufficient combined with Socialism. She
also felt the call for self determination was a \"paraphrase of the old
slogan of bourgeois nationalism\", that the opposition to national
oppression does not come from any special right of nations, but of the
duty to oppose the class regime and every form of social inequity and
social domination. This was the position that Bukharin would support,
along with a few other allies of his. Lenin would be outraged and would
demand the journal Radek published his opposing view be abolished. Lenin
would ban their ability to communicate with Bolshevik sections in
Russia.

The Bolsheviks and the World War by Olga Gankin starting on page 219 has
the thesis in English, Now the actual Theses is nearly 3 pages long, and
given this video is not about Bukharin-Piatakov groups position on it. I
am not going to read it in full, but hit on the main points

\"It is therefore impossible to struggle against the enslavement of
nations otherwise than by struggling against imperialism, ergo-by strug-
gling against imperialism, ergo-by struggling against finance capital,
ergo against capitalism in general. Any deviation from that road, any
advancement of \"partial\" tasks, of the \"liberation of nations\"
within the realm of capitalist civilization, means the diverting of
proletarian forces from the actual solution of the problem, and their
fusion with the forces of the corresponding national bourgeois groups.

The slogan of \"self-determination of nations\" is first of all Utopian
(it cannot be realized within the limits of capitalism) and harmful as a
slogan which disseminates illusions. In this respect it does not differ
at all from the slogans of the courts of arbitration, of disarmament,
etc., which presuppose the possibility of so-called \"peaceful
capitalism.

\...

To struggle against the chauvinism of the working masses of a Great
Power by means of the recognition of the right of nations for
self-determination, is equivalent to struggling against this chauvinism
by means of the recognition of the right of the oppressed \"fatherland\"
to defend itself\"

Bukharin in other articles also put forth that breaking up the working
masses by this right to defend the fatherland would weaken the
revolution, which is not dissimilar of the position Rosa would put forth
in \"The Russian Revolution\" when she was critical of the Bolsheviks on
the national question.

\"It is obvious that the phrases concerning self-determination and the
entire nationalist movement, which at present constitute the greatest
danger for international socialism, have experienced an extraordinary
strengthening from the Russian Revolution and the Brest negotiations. We
shall yet have to go into this platform thoroughly. The tragic fate of
these phrases in the Russian Revolution, on the thorns of which the
Bolsheviks were themselves, destined to be caught and bloodily
scratched, must serve the international proletariat as a warning and
lesson.\"

It is something I found interesting and I am getting a bit ahead of
myself, but after the Revolution some Bolsheviks basically turned around
on this Slogan. Stalin is a great example of this,

In 1918 Stalin declared the slogan of self-determination was outmoded,
and by 1920

\"the demand for the secession of the border regions from Russia .. must
be rejected not only because it runs counter to the very formulation of
the question of establishing a union between the centre and the border
regions, but primarily because it runs fundamentally counter to the
interests of the mass of people in both the centre and border regions\"

So while Stalin who was previously for it basically turned against it,
in practice Bukharin ended up in some cases actually being quite
defensive of oppressed groups. During the Georgian affair the Russian
Bolsheviks ended up in conflict against the Georgian Bolsheviks, a dying
Lenin upset by the affair asked Trotsky to defend them at the
12congress, while Trotsky initially turned him down he actually ended up
attempting to defend them during the preparations for the congress, in
this only Bukharin supported Trotsky. When it came to the congress
Bukharin was the only one to make a major defense of the Georgian
Bolsheviks. He also declared that the national question was of prime
importance to the soviet state and that all questions at their root tied
back into the national question, and there must be special concessions
made to oppressed nations. This is after 1917 of course but I found it
interesting.

Anyway as well during 1917 at the Seventh Party conference Piatakov
tried to change the programme and get the slogan of self-determination
declared to be counter revolutionary, Lenin would work out a compromise
position with Bukharin against Piatakov. [{% cite smith1999bolsheviks %} 20]

Anyway I have got ahead of my self talking about things that happened
much later, let get back to where we were.

1916
====

These differences would grow over 1916 between a lot of younger
Bolsheviks who sided with Rosa, Bukharin and Radek's writings, Lenin
declared their ideas \"have nothing in common wither with Marxism or
revolutionary social democracy\" [{% cite cohen1973bukharin %} 37]

Now this seems rather harsh, but I think this touches on what i
mentioned earlier and I am going to quote directly from Cohen's
biography of Bukharin because I'm kind of having a hard time rephrasing
it and I think it puts it well

\"The leader's attitude confirms the impression that the \"closer men
were to Lenin, the more bitterly he quailed with them\" For even during
the worst period in their relationship, furtive evidence of their
underlying mutual affection now and then appeared. Bukharin occasionally
tired to appeal to that feeling. He begged Lenin not \"to publish
against me the kind of article that makes it impossible for me to answer
cordially \... I did not want and do not want \... a split\" Lenin was
not totally unperceptive. In April 1916, Bukharin was arrested in
Stockholm for his participation in an anti-war socialist congress.
Learning of his trouble, Lenin dispatched an urgent appeal for help; and
later in April, after Bukharin had been deported to Oslo, Lenin wrote to
another Bolshevik in Norway asking him to convey best regards to
Bukharin: \"I hope from my heart that he will very soon take a rest and
be well. How are his finances?\" The message was terse, but, under the
circumstances, warm, even fatherly. The benignity was short lived. By
July, Lenin was explaining to Zinoviev that \"I am not so ill-disposed
towards Bukharin, I cannot write\"

Debate on the Marxist Theory of the State
=========================================

In early 1916 Lenin requested from Bukharin an article on economics,
however he produced \"Towards a Theory of the Imperialist State\"
sending it to Lenin to be published.

This portion of the script got so long I ended up turning it into its
own video I will summarize here, but you should go watch my video
Bukharin Vs Lenin on the Marxist Theory of the state.

Lenin initially considered publishing it as a discussion article, but
found it too incorrect on the question of the state. Now without fully
going into the history of this, Kautsky and many prominent other
\"Marxists\" of this era had completely dropped any idea of the smashing
of the bourgeois state and the construction of a new workers state or a
dictatorship of the proletariat, this was a product of them capitulating
and becoming reformists, and so with that they had to uphold the idea of
just taking power in the bourgeois state. Bukharin was going against the
most common Marxist understanding the state and revolution at the time.

Lenin felt that Bukharin had taken Engels out of context, and
specifically his idea that Anarchists and Marxists do no differ on the
state as being very incorrect. Lenin even went as far to accuse him of
semi-anarchism, and said he fully downplayed the need for a state post
revolution. [{% cite cohen1973bukharin %} 39-40] However Bukharin did not ignore
this, \"the proletariat destroys the state organization of the
bourgeoisie, takes over its material framework, and creates its own
temporary organization of state power\"

This debate would be carried out in letters, which at least according to
what I can find have never been published So we can't really take a look
at those unfortunately. [{% cite cohen1973bukharin %} 398] This debate would act
as a sort of catalyst for Lenin to take a deeper look at the state in
the works of Marx and Engels.

During this debate Bukharin mood was impacted and he fell into lower
spirits, Bukharin made the choices to take a steamer to America, this
was at the recommendation of a friend and fellow Bolshevik Alexander
Shlyapnikov. Shlypanikov told him he should go write for a newspaper
Alexandra Kollontai edited for in New York [{% cite allen2016alexander %} 72] When
Lenin heard about this became deeply worried that he had driven Bukharin
away, he inquired someone to find out with what mood is Bukharin leaving
and will he continue to write to Lenin and others, and still fill
requests for other documents. Lenin would then receive Bukharins
farewell letter. I am going to quite in full the closing plea that Cohen
includes from Bukharin's letter.

\"I ask one thing of you: if you must polemicize, ect., preserve such a
tone that it will not lead to a split. It would be very painful for me,
painful beyond endurance, if joint work, even in the future, should
become impossible. I have the greatest respect for you; I look upon you
as my revolutionary teacher and love you.\"

Lenin would respond saying while the charges were valid, he gave
Bukharin praise and said \"We all value you highly.\" Bukarhin would
respond \"Be well, think kindly of me \.... I embrace you all,\"

In December of 1916 Lenin wold say he was working towards producing an
article of his own on it. To do this Lenin started gathering all the
works of Marx and Engels he could and rereading it all. Lenin wrote and
prepared a notebook from January and February, these notes would form
the basis of what would become The State and Revolution. Lenin after
examining the question concluded that Kautsky was far more wrong then
Bukharin, but he still felt that Bukharin was wrong about a few things.
Then in May Krupskaya told Bukharin that Lenin no longer had any
differences with him on the question of the state. According to Cohen it
is possible in a letter or through someone else Bukharin might have been
made aware of Lenin's shift in opinion earlier.

In July Lenin had to go into hiding and told Kamenev if the provisional
government was able to kill him that Kamanev was to publish the notes.
Lenin managed to evade capture and turned his notes into The State and
Revolution while hiding in Finland. It was originally planned to be
published in 1917, but the revolution put a hold and it was not to be
published until January 1918.

Bukharin in America
===================

But back to Bukharin in America.

Bukharin in November of 1916 arrived in New York, in January of 1917 he
become editor of a Russian language newspaper. He also advocated and
organized American socialists around the line of the Zimmerwald left,
for information on the Zimmerwald left see my Rosa Luxemburg video. Also
in January Leon Trotsky also in the US would join the editorial staff.
[{% cite cohen1973bukharin %} 43-44]

A story that I think shows more of Bukharins personal character comes
from when Trotsky and his wife met Bukharin in New York, this is from
Kenneth D. Ackerman's Trotsky in New York.

"Bukharin greeted us with a bear-hug," she wrote. Added Trotsky, he
"welcomed us with the childish exuberance characteristic of him.\"
Bukharin had found something in New York City that he felt Trotsky, as
Europe's foremost socialist writer, would surely appreciate. It wasn't
the theater or the skyscrapers; not the subway, the cinema, or the fancy
stores. Instead, \"We had hardly got off the board when he told us
enthusiastically about a public library which stayed open late at night
and which he proposed to show us at once,\" Natalya recalled. \"At about
nine o'clock in the evening we had to make the long journey to admire
his great discovery.\"

\...

At Fifth Avenue, Bukharin led them around the corner until they stood in
front of a great white marble building, an architectural marvel opened
just a few years earlier, in 1911. Two white marble lions guarded the
front entrance from either side. Overhead, etched in stone, was the name
New York Public Library.

Bukharin knew Trotsky would adore this site.

\...

Bukharin took them inside and led them up marble stairways to the
building's top floor, then through a small foyer to the library's main
reading room. This too was magnificent, a vast open space almost three
hundred feet long and seventy-seven feet wide, larger than the entire
ship Montserrat on which they had just crossed the ocean, which ceiling
paintings and sculptures and flooded with light. and books! The
library's seventy-five miles of shelves held more than a million of
them, plus newspapers and magazines from around the world! For anyone!
For Free! To just come and read! Till almost midnight! Even on a Sunday
night!\" [{% cite ackerman2016trotsky %} 38-39]

I find this story like of cute, of the first thing Bukharin wanted to
show someone was the public library.

Trotsky and Bukharin were to debate the direction the American Socialist
Party should take, with Bukharin arguing for a split, where Trotsky
argued for staying and kicking out the reactionary elements. While they
debated this they maintained a warm friendship and politically
collaborated on other issues. [{% cite cohen1973bukharin %} 43-44]

In March Bukharin and the rest of the exiled Russians in New York would
be made aware of the February Revolution and the abdication of the Tsar,
Bukharin like many would immediately try to return to Russia. He set
sail in April, he was detained first in Japan for a week, then by the
local Menshevik government and he only managed to reach Moscow by may.

Bukharin's reaction to 1917
===========================

Now I want to cover Bukharin's analysis of the Russian Revolution in
1917 so we can contrast it with both Lenin's and the positions of the
Bolsheviks in Russia.

This is from The Russian Revolution and Its Significance, this was
published in June, but the positions are reflected in Bukharins earlier
work.

\"Everything points to a compromise between the ruling classes. The
revolution was not yet strong enough to overthrow the capitalist system;
it has only effected a shifting of the elements within the bourgeoisie
as a whole, has placed the more progressive wing at the helm, by pushing
aside the reactionary nobility.

But the revolution is steadily growing. Even now, while these lines are
being written, there exist in Petrograd two governments, one, that of
the Imperialist bourgeoisie, which was jubilantly greeted by the
bourgeois classes of the other allied nations; the other, the
governmental machine of the proletariat, the workingmen's and soldiers'
council.

The struggle between the working class and the Imperialists is
inevitable. Even the reforms that have been proclaimed by the
provisional government were concessions made out of fear of the threats
of the proletariat. But the liberal government will not be in a position
to fulfill the programme that has been forced upon it.

\...

But the conquest of political power by the proletariat will, under the
existing circumstances, no longer mean a bourgeois revolution, in which
the proletariat plays the role of the broom of history. The proletariat
must henceforth lay a dictatorial hand upon production, and that is the
beginning of the end of the capitalist system.

A lasting victory of the Russian proletariat is, however, inconceivable
without the support of the west European proletariat.

\...

To-day the bourgeoisie stands at its grave. It has become the citadel of
reaction. And the proletariat has come to destroy its social order.

The call to arms to this great upheaval is the Russian Revolution. Well
may the ruling classes tremble before a communist revolution. The
proletariat has nothing to lose but its chains; it has a world to gain.
\"

Now from Lenin, The Dual Power.

\" The highly remarkable feature of our revolution is that it has
brought about a dual power. This fact must be grasped first and
foremost: unless it is understood, we cannot advance. We must know how
to supplement and amend old "formulas", for example, those of
Bolshevism, for while they have been found to be correct on the whole,
their concrete realisation has turned out to be different. Nobody
previously thought, or could have thought, of a dual power.

What is this dual power? Alongside the Provisional Government, the
government of bourgeoisie, another government has arisen, so far weak
and incipient, but undoubtedly a government that actually exists and is
growing---the Soviets of Workers' and Soldiers' Deputies.

\...

Should the Provisional Government be overthrown immediately?

My answer is: (1) it should be overthrown, for it is an oligarchic,
bourgeois, and not a people's government, and is unable to provide
peace, bread, or full freedom;\"

and from The April Theses

\"The specific feature of the present situation in Russia is that the
country is passing from the first stage of the revolution---which, owing
to the insufficient class-consciousness and organisation of the
proletariat, placed power in the hands of the bourgeoisie---to its
second stage, which must place power in the hands of the proletariat and
the poorest sections of the peasants. \"

So both Bukharin and Lenin recognized that the soviets were the
proletarian government and that it must defeat the provisional
government.

In March of 1917 Pravada was edited by Molotov and the Bureau of the
Central Committee remained under control of Alexander Shliapnikov, they
advocated for no trust in the provisional government and for a socialist
revolution. [{% cite rabinowitch1991prelude] %}

Shliapnikov held this position because he was the highest ranking member
in Petrograd, but not for long Kamanev and Stalin would return and they
seized control of Pravada, Kamanev put forth more moderate proposals in
the name of the Bolsheviks and Shliapnikov not wanting to show any
disunity in the party decided not to fight him. [{% cite allen2016alexander %} 80]

The moderate wing of the party now headed by Kamanev and Stalin refused
to print Lenin's letters from afar except for one and deleted sections
condemning the provisional government, as well Stalin proposed
reunification with the Mensheviks. Stalin as well as others felt there
would be decades between the bourgeois revolution and the proletarian
revolution revolution.[{% cite tucker1974stalin %} 167-168]

On April 6Kamenev supported by Stalin attacked the April Theses, and on
the 8Kamenev in Pravda declared that the April Theses was just Lenin's
personal opinion. However by late April at the Seventh All-Russian
Conference of the RSDLP some of Lenin's proposals were accepted, though
the right wing of the party managed to get a lot of the members on the
Central Committee. [{% cite rabinowitch1991prelude %} 44-46]

However many other Bolsheviks would oppose socialist revolution for
months and some even up until the October Revolution.

It should be noted that Lars Lih rejects this description of events in
his work The Ironic Triumph of Old Bolshevism, and I read it and really
found it unconvincing, but it might be something you want to give a read
if you want to hear an alternative theory on this, but I really do think
Lars Lih is wrong.

I also find Bukharin's position of

\"The liberal government will not be in a position to fulfill the
programme that has been forced upon it. \... But the conquest of
political power by the proletariat will, under the existing
circumstances, no longer mean a bourgeois revolution, in which the
proletariat plays the role of the broom of history.\"

To really be quite similar to Trotsky's theory of permanent revolution,
the idea that the bourgeois government can't carry out its tasks and
programme, so the revolution will go over into a proletarian revolution.

Bukharin back in Moscow
=======================

This was the background of which Bukharin was returning, while outside
of Russia him and Lenin had many disagreements, many of them gone due to
Lenin coming over to Bukharin's position such as the state or they lost
their importance with the revolution. Lenin was primarily concerned with
getting the party against the provisional government and supporting
socialist revolution. Lenin of course was not alone in this struggle,
many new workers who had joined the party were very radical and wanted
the Soviets to take power.

In 1915 Bukharin had called on Lenin to work with anti-war social
democrats built around Trotsky, and in a way in 1917 Lenin would support
this.[{% cite cohen1973bukharin %} 47], in May Lenin would offer Trotsky and his
followers to join the Bolsheviks and offered them leadership roles and
editorial positions on Pravda.[{% cite deutscher2003prophetarmed %} 257] While he
would not end up formally joining until the Bolshevik Sixth Congress
when the time came it fell to Bukharin to welcome Trotsky and his
followers to the party. As well with this occurring during the July
days, Bukharin alongside Stalin delivered most of the congresses main
speeches, and Bukharin would join the Bolshevik Central Committee.
[{% cite cohen1973bukharin %} 47-49]

Bukharin also was one of the primary figures in the struggle to
radicalize the Bolsheviks for insurrection in Moscow, most histories of
the revolution tend to focus fully on Petrograd, Moscow is mostly
ignored. I have wanted to do a video on the events of Moscow during the
October Revolution, but where there is tons of information on Petrograd
there is rather little on Moscow. This also means detailed information
on what Bukharin was up to during this time is not easy for me to get a
hold of.

Bukharin with many of his friends would take control of the party
newspapers there and with Bukharin on the editorial board they became a
strong voice for revolution, this combined with the efforts of many
other Bolsheviks won the local party members to revolution. On November
6th the Moscow Soviet would begin organizing Red Guards while facing
opposition from the Mensheviks and SRs. The revolution in Moscow unlike
in Petrograd was not bloodless, the city Duma had prepared a Committee
of Public Safety to counter the revolution. There was over a week of
bloody street fighting, and on November 10the Junkers alongside the
Committee of Pubic Safety seized the cities railways stations telephone
stations and surrounded the Kremlin the commander of the Kremlin
surrendered it on the promise his men would be would be spared, when
Berzin the commander let them the junkers killed him. In the coming
morning the workers of the Kremlin arsenal were ordered to line up in
the courtyard. I am quoting from a first hand account Victor Serge
includes in his Year One of the Russian Revolution.

\"The men still cannot believe that they are going to be shot like this,
without trial, without sense they have taken no part in the fighting. A
command bellow out: 'In line now! Eyes Front!' The men stand rigid,
fingers along the seams of their trousers. At a signal, the din of the
three machine-guns blends with cried of terror, sobs and death rattles.
All those who are not mown down by the first shots dash towards the only
exist, a little door behind them which had been left open. The
machine-guns carry on firing; in a few minutes the doorway is blocked by
a heap of men, lying there screaming and bleeding into which the bullets
still rain \... The walls of the surrounding buildings are spattered
with blood and bits of flesh\"

This was not the only mass execution carried out in Moscow during the
fighting, the forces against the revolution carried out others.
Eventually though the Military Revolutionary Committee and the Moscow
Soviet would be victorious after 9 days, the committee of public safety
surrendered on the condition they bet let free, and the Military
Revolutionary Committee would honor this and they were permitted to go
free.[{% cite sergeyearone %} 80-85]

and a direct quote again from Cohen's biography of Bukharin

\"The bloody fighting in Moscow, where five hundred Bolsheviks alone
died (compared to a total of only six people in Petrograd), may already
have alerted Bukharin to the impending \"costs of revolution.\" Stukov
later recalled their mood when he and Bukharin arrived in Petrograd to
report on their victory: \"When i started to speak about the number of
victims something welled up in my throat and I stopped. I see Nikolai
Ivanovich throwing himself on the chest of a bearded worker, and they
start to sob. People start to cry.\" The real revolution had
begun.\"[{% cite cohen1973bukharin %} 59]

Part 1 Conclusion
=================

and with that we have covered Bukharin's life prior to revolution, he
would go onto to a ton of things in the 1920s, which I fully plan to
cover however in a separate video in order to keep this was from being
excessively long, the good news is I have it like 80 percent written
already and it shouldn't be that long between this video and the next
one, I also might do a separate video on just Bukharin and Trotsky in
New York given the impact they had on the American left.

I hope you liked it, please subscribe for more content, let me know if
you like this attempt at doing a bit more visually with this video as
well as audio. Please do share it as the Youtube algorithm does not for
me.

{% bibliography --cited %}

