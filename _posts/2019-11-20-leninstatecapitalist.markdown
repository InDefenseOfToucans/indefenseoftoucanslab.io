---
layout: post
author:
- InDefenseOfToucans
bibliography:
- sources.bib
date: '2019-11-20'
title: 'Did Lenin Consider the Soviet Union State Capitalist?'
---

Introduction
============

A persistent claim I see going around is that Lenin himself declared
that the Soviet Union was Capitalist. Now I don't want this video to be
a discussion on what the Soviet Union was, but purely a look at what
Lenin himself considered it, and not a question on if it is accurate or
not.

So let us dive into the question, what did Lenin consider the Soviet
Union.

How Did Lenin Describe it?
==========================

I think this quote from a speech of his given at the Third All-Russia
Congress Of Soviets Of Workers', Soldiers' And Peasants' Deputies makes
it fairly clear.

\"I have no illusions about our having only just entered the period of
transition to socialism, about not yet having reached socialism\... We
are far from having completed even the transitional period from
capitalism to socialism. We have never cherished the hope that we could
finish it without the aid of the international proletariat. We never had
any illusions on that score, and we know how difficult is the road that
leads from capitalism to socialism. But it is our duty to say that our
Soviet Republic is a socialist republic because we have taken this road,
and our words will not be empty words.\"

I think this might confusing to some people, those who think because it
was not Socialism then it must be Capitalism, you might think of it this
way, but it is certainly not in line with Marx, Engels, Lenin or many
other Bolsheviks thought of it. They thought there was a transitional
period which I will show with later quotes.

Lenin also talks about the economic features of this transition.

From Lenin's Economics And Politics In The Era Of The Dictatorship Of
The Proletariat

\"Theoretically, there can be no doubt that between capitalism and
communism there lies a definite transition period which must combine the
features and properties of both these forms of social economy. This
transition period has to be a period of struggle between dying
capitalism and nascent communism---or, in other words, between
capitalism which has been defeated but not destroyed and communism which
has been born but is still very feeble.

\...

Socialism means the abolition of classes. The dictatorship of the
proletariat has done all it could to abolish classes. But classes cannot
be abolished at one stroke.

And classes still remain and will remain in the era of the dictatorship
of the proletariat. The dictatorship will become unnecessary when
classes disappear. Without the dictatorship of the proletariat they will
not disappear.

Classes have remained, but in the era of the dictatorship of the
proletariat every class has undergone a change, and the relations
between the classes have also changed. The class struggle does not
disappear under the dictatorship of the proletariat; it merely assumes
different forms. \"

We will also take a look at Lenin's State and Revolution.

\"Furthermore, during the transition from capitalism to communism
suppression is still necessary, but it is now the suppression of the
exploiting minority by the exploited majority. A special apparatus, a
special machine for suppression, the "state", is still necessary, but
this is now a transitional state. \"

Lenin talk of this transitional state needed between Capitalism, and
Communist Society both the lower and higher phase. He also talks of this
transitional period having features of both capitalism and socialism.
The title of the work \"Economics And Politics In The Era Of The
Dictatorship Of The Proletariat\" is no accident, Lenin is talking of
the features found under the Dictatorship of the Proletariat.

Now I want to show from the works of other Bolsheviks and from Marx that
they too agreed with Lenin on this formulation.

From Bukharin and Preobrazhensky's ABC of Communism.

\"Under the dictatorship of the proletariat (a temporary institution)
the means of production will from the nature of the case belong, not to
society as a whole, but only to the proletariat, to its State
organization. For the time being, the working class, that is the
majority of the population, monopolizes the means of production.
Consequently there does not yet exist communist production in all its
completeness. There still exists the division of society into classes\"

We can see this connection to Karl Marx

\"Between capitalist and communist society there lies the period of the
revolutionary transformation of the one into the other. Corresponding to
this is also a political transition period in which the state can be
nothing but the revolutionary dictatorship of the proletariat.\"

To summarize, once the Proletariat has seized power it will establish a
transitional state, or a workers state. This is the era of the
Dictatorship of the Proletariat. Important to emphasize that when Marx
and others use the term \"dictatorship\" it does not mean a literal
dictator over society, but that the state represents an interest of a
particular class, and its property relations, in the case of a workers
state the proletariat, in the case of the bourgeois state the capitalist
class. During this era there is going to be a period of transition, and
there is going to be features of both the growing Socialism, and the
remaining aspects of Capitalism.

Another important aspect of Lenin's thought on this is the aspect of how
Russian Empires development was uneven. Lets take a look at another
quote from the Third All-Russia Congress.

\"Comrades, no socialist would refuse to admit the obvious truth that
between socialism and capitalism there lies a long, more or less
difficult transitional period of the dictatorship of the proletariat,
and that the forms this period will take will be determined to a large
extent by whether small or big ownership, small or large-scale farming,
predominates. It goes without saying that the transition to socialism in
Estland, that small country in which the whole population is literate,
and which consists of large-scale farms, cannot be the same as the
transition to socialism in Russia, which is mainly a petty-bourgeois
country. This must be taken into account.\"

Lenin focusing specifically due to agriculture that the development of
Socialism in Russia would be different, that much of it remained
petty-bourgeois, Russia had not developed the large farms with lots of
hired labor as other nations did. This means it's transition period is
longer and will have aspect of capitalism in its economy for longer.

So we have a foundation of what Lenin thought, lets now take a look at a
few of the quotes people often cite as being proof Lenin thought the
Soviet Union was State Capitalist.

The Claims of Lenin Calling it State Capitalist
===============================================

I am pulling two of the quotes from a post on Libcom titled \"Lenin
acknowledging the intentional implementation of State Capitalism in the
USSR\" So I am not strawmanning or anything these are quotes people cite
for this argument.

Let us start with this quote from \"To the Russian Colony in North
America\"

\"The state capitalism, which is one of the principal aspects of the New
Economic Policy, is, under Soviet power, a form of capitalism that is
deliberately permitted and restricted by the working class. Our state
capitalism differs essentially from the state capitalism in countries
that have bourgeois governments in that the state with us is represented
not by the bourgeoisie, but by the proletariat, who has succeeded in
winning the full confidence of the peasantry.

Unfortunately, the introduction of state capitalism with us is not
proceeding as quickly as we would like it. For example, so far we have
not had a single important concession, and without foreign capital to
help develop our economy, the latter's quick rehabilitation is
inconceivable.

Those to whom the question of our New Economic Policy---the only correct
policy---is not quite clear, I would refer to the speeches of Comrade
Trotsky and my own speech at the Fourth Congress of the Communist
International\[1\] devoted to this question.\"

So Lenin talks about it as an aspect of the New Economic Policy, and
this leads us well into the other quote from that post from libcom. They
are pulling a single two paragraph line in the Tax in Kind where Lenin
is quoting himself, from an earlier work.

Let us look at the quote they are talking about.

\"State capitalism would be a step forward as compared with the present
state of affairs in our Soviet Republic. If in approximately six months'
time state capitalism became established in our Republic, this would be
a great success and a sure guarantee that within a year socialism will
have gained a permanently firm hold and will have become invincible in
this country.\"

They argue see Lenin is saying State Capitalism is better and the Soviet
Republic must be State Capitalist, but this is really not want Lenin is
saying at all. Let us look at when Lenin says further.

\"No one, I think, in studying the question of the economic system of
Russia, has denied its transitional character. Nor, I think, has any
Communist denied that the term Soviet Socialist Republic implies the
determination of the Soviet power to achieve the transition to
socialism, and not that the existing economic system is recognised as a
socialist order.

But what does the word "transition" mean? Does it not mean, as applied
to an economy, that the present system contains elements, particles,
fragments of both capitalism and socialism? Everyone will admit that it
does. But not all who admit this take the trouble to consider what
elements actually constitute the various socio-economic structures that
exist in Russia at the present time. And this is the crux of the
question.

Let us enumerate these elements:

(1)patriarchal, i.e., to a considerable extent natural, peasant farming;

(2)small commodity production (this includcs the majority of those
peasants who sell their grain);

(3)private capitalism;

(4)state capitalism;

(5)socialism.

Russia is so vast and so varied that all these different types of
socio-economic structures are intermingled. This is what constitutes the
specific feature of the situation.

The question arises: What elements predominate? Clearly, in a
small-peasant country, the petty-bourgeois element predominates and it
must predominate, for the great majority---those working the land---are
small commodity producers. The shell of state capitalism (grain
monopoly, state-controlled entrepreneurs and traders, bourgeois
co-operators) is pierced now in one place, now in another by profiteers,
the chief object of profiteering being grain.\"

So this ties back to what was talked about earlier, Lenin is talking
about the ways that they are going though this and that it has a
transitional character, that Russia at this time had aspects of
capitalism, and of socialism.

It becomes even more clear later

\"It is because Russia cannot advance from the economic situation now
existing-here without traversing the ground which is common to state
capitalism and to socialism (national accounting and control) that the
attempt to frighten others as well as themselves with "evolution towards
state capitalism" is utter theoretical nonsense. This is letting one's
thoughts wander away from the true road of "evolution", and failing to
understand what this road is. In practice, it is equivalent to pulling
us back to small proprietary capitalism.\"

Lenin talks of the specific aspects of state capitalism on which they
are walking on, it is the ground which is common to state capitalism and
to socialism. But you still might say why walk this road at all why not
just build Socialism. Well Lenin talks about this in relation to Germany
and other countries where things are more developed.

\"A victorious proletarian revolution in Germany would immediately and
very easily smash any shell of imperialism (which unfortunately is made
of the best steel, and hence cannot be broken by the efforts of any
chicken) and would bring about the victory of world socialism for
certain, without any difficulty, or with only slight difficulty---if, of
course, by "difficulty" we mean difficulty on a world historical scale,
and not in the parochial philistine sense.

\...

At present petty-bourgeois capitalism prevails in Russia, and it is one
and the same road that leads from it to both large-scale state
capitalism and to socialism, through one and the same intermediary
station called "national accounting and control of production and
distribution". Those who fail to understand this are committing an un
pardonable mistake in economics. Either they do not know the facts of
life, do not see what actually exists and are unable to look the truth
in the face, or they confine themselves to abstractly comparing
"socialism" with "capitalism" and fail to study the concrete forms and
stages of the transition that is taking place in our country.\"

Russia as it was then petty-bourgeois capitalism dominated, and as part
of the transitional to socialism it is going to walk ground common to
state capitalism. A big part of the NEP is the idea of peasants selling
grain, but they sell to the state not to international markets, this is
the state monopoly on trade which is a key aspect of the state
capitalism which is but one aspect of the wider economy as Lenin says.

In the \"To the Russian Colony in North America\" Lenin says \"I would
refer to the speeches of Comrade Trotsky and my own speech at the Fourth
Congress of the Communist International\" So let us listen to Lenin and
go look at these speeches, sense these are dedicated to explaining why
this is the path that must be walked.

Let us look at two paragraphs from Trotsky's The Economic Situation of
Soviet Russia from the Standpoint of the Socialist Revolution

\"The assertion of the Social Democrats to the effect that the soviet
state has "capitulated" to capitalism is thus an obvious and crass
distortion of the reality. As a matter of fact the Soviet government is
following an economic path which it would doubtless have pursued in
1918-19 had not the implacable demands of the civil war obliged it to
expropriate the bourgeoisie at one blow, to destroy the bourgeois
economic apparatus and to replace the latter hastily by the apparatus of
War Communism.

\...

The inclusion of the peasantry in planned state economy, that is,
socialist economy, is a task far more complicated and tedious.
Organizationally the way is being paved for this by the state-controlled
and state-directed co-operatives, which satisfy the most pressing needs
of the peasant and his individual enterprise. Economically this process
will be speeded up all the more, the greater is the volume of products
which the state industry will be able to supply to the village through
the medium of co-operative societies. But the socialist principle can
gain complete victory in agriculture only through the electrification of
agriculture which will put a salutary end to the barbaric disjunction of
peasant production. The electrification plan is therefore an important
component part of the overall state economic plan; and because its
importance will doubtless increase in proportion to the growing
productive forces of Soviet economy it is bound to gain in ascendancy in
the future, until it becomes the basis for the overall socialist
economic plan as a whole.\"

Here Trotsky engages directly with the accusation that the soviet state
was turning to capitalism, turns out this criticism is not new. I don't
want to turn this video into a why the NEP was good and a history of it
which I plan to do someday, just not today. But we can see these
economic policy this aspect of the economy that is capitalist is with
the peasantry, and why well because integrating the peasantry into the
socialist economy is complicated and slow. Trotsky points to the
development of the cooperatives and electrification being vital in the
efforts to integrate them into it.

We should also take a look at Lenin's speed from the Fourth Congress of
the Comintern.

\"Now that I have emphasised the fact that as early as 1918 we regarded
state capitalism as a possible line of retreat, I shall deal with the
results of our New Economic Policy. I repeat: at that time it was still
a very vague idea, but in 1921, after we had passed through the most
important stage of the Civil War---and passed through it
victoriously---we felt the impact of a grave---I think it was the
gravest---internal political crisis in Soviet Russia. This internal
crisis brought to light discontent not only among a considerable section
of the peasantry but also among the workers. This was the first and, I
hope, the last time in the history of Soviet Russia that feeling ran
against us among large masses of peasants, not consciously but
instinctively. What gave rise to this peculiar, and for us, of course,
very unpleasant, situation? The reason for it was that in our economic
offensive we had run too far ahead, that we had not provided ourselves
with adequate resources, that the masses sensed what we ourselves were
not then able to formulate consciously but what we admitted soon after,
a few weeks later, namely, that the direct transition to purely
socialist forms, to purely socialist distribution, was beyond our
available strength, and that if we were unable to effect a retreat so as
to confine ourselves to easier tasks, we would face disaster.\"

Lenin I think gets to the heart of the manner, transitioning directly to
a socialist economy was not possible in Russia, in other documents Lenin
talks of other countries like Germany might be able to make that leap,
but not Russia.

What Other Option?
==================

This ties well into a point made in my video on Rosa where she is
critical of the Bolsheviks for giving the peasants land which was a
capitalist policy. But as Lenin points out attempts to push forward
towards socialism in the country side caused a crisis. So I have always
been curious what is the other option that could have been taken, what
should have been done rather then a compromise with the peasantry?
Should it have been forced on the peasantry, I think anyone looking at
the historical facts any attempt to force all of the peasants into
collective farms would have drown the young Soviet Republic in the blood
of millions of peasants.

Conclusion
==========

So to reiterate the main points, Lenin and the Bolsheviks saw the Soviet
Republic as a state in transition from capitalism to socialism, that it
was a workers state, which is qualitatively different from a bourgeois
state you have under capitalism. That the Soviet Economy had many
different and diverse elements, some of which are in common with
Capitalism and some with Socialism. That this republic would retain
planning and control over the factories, and there would be a state
capitalism over the petty-bourgeois capitalism of the country side,
handling accounting, and retaining a monopoly on foreign trade to
insulate the internal capitalists from external capitalism. No where do
they argue the state is capitalist, that it is a bourgeois state, Lenin
and others are firm in this being a Dictatorship of the Proletariat.
This is consistent point among Lenin's writings and speeches. This is
all very clear if you actually read his writings, rather then hunting
through Lenin's writings to grab a few quotes that might look to agree
with your point at first glance.
