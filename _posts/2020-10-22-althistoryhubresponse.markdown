---
layout: post
author:
- InDefenseOfToucans
bibliography: 'sources.bib'
date: October 22 2020
title: |
    Let Us Take a Look at AltHistoryHub's "What if Trotsky Came To Power
    Instead Of Stalin?"
---

<iframe width= "100%" height= "400px" sandbox="allow-same-origin allow-scripts allow-popups" src="https://watch.breadtube.tv/videos/embed/ae6618b9-9fd8-4e65-9bb2-abbcbd0ffcd3" frameborder="0" allowfullscreen></iframe>

[This video can also be watched on Youtube](https://youtu.be/mPR-0lkw8ps)

[Click here for the PDF version of this script]({{ site.url }}/assets/scripts/Let_Us_Take_a_Look_at_AltHistoryHub_s__What_if_Trotsky_Came_To_Power_Instead_Of_Stalin.pdf)

Introduction
============

Welcome to another response video, the thing I said I would never do
again oops, but I saw people link the althistoryhub video in a serious
political discussion and decided I needed to make a sort of response to
it.

So, I do recognize that Alt History topics on the whole are more for
entertainment, I also recognize given the point of divergence is
pre-1917 in his scenario really any change could be justified. My hope
is that my video response could educate and let people could understand
why I think this video is entertainment and not even infotainment.

For this reason we will mostly be talking about the start of the video
as the later part is mostly pure speculation as alt-history is, and some
of the parts of the video are feelings and morality based, so I don't
really care to go over those, I am a Communist and AltHistoryHub I
assume is a Liberal of some flavor and so of course I support workers
revolutions, where he would not, I won't spend much time on that at all,
but rather on the historical facts.

Before getting into the facts of the video, I kind of want to first I
want to respond to people linking this in more serious political
environment, I have had it sent to me as if it was relevant politically
and I really dislike Trotsky as leader of the USSR as an alt-history
scenario, especially because I think people confuse it was somehow being
an important question to Trotskyism, the person who linked it to me
thought it debunked Trotskyism as an ideology, that if Stalin replaced
with Trotsky wouldn't have done better then Trotskyism debunked. But
that is not really the underpinning of Trotskyism as an ideological
movement. Even when Trotsky was alive people kind of brought up this
sort of scenario, and he dismisses it.

>\"Even now, in spite of the dramatic events in the recent period, the
average philistine prefers to believe that the struggle between
Bolshevism (\"Trotskyism\") and Stalinism concerns a clash of personal
ambitions, or, at best, a conflict between two \"shades \" of
Bolshevism. The crudest expression of this opinion is given by Norman
Thomas, leader of the American Socialist Party: \"There is little reason
to believe\". he writes (Socialist Review, September 1937, p. 6), \"that
if Trotsky had won (!) instead of Stalin, there would be an end of
intrigue, plots, and a reign of fear in Russia\". And this man considers
himself \... a Marxist. One would have the same right to say: \"There is
little reason to believe that if instead of Pius XI, the Holy See were
occupied by Norman I, the Catholic Church would have been transformed
into a bulwark of socialism\". Thomas fails to understand that it is not
a question of antagonism between Stalin and Trotsky, but of an
antagonism between the bureaucracy and the proletariat.\"

Trotsky was also asked why didn't he use his Military connections to
coup Stalin, and his answer was

>\"There is no doubt that it would have been possible to carry out a
military coup d'état against the faction of Zinoviev, Kamenev, Stalin,
etc., without any difficulty and without even the shedding of any blood;
but the result of such a coup d'état would have been to accelerate the
rhythm of this very bureaucratization\"

So Trotsky said just replacing Stalin with him would not have reversed
course, he compares this to asking if putting a Socialist in charge of
the Catholic Church would make it a bulwark of socialism. Trotsky
himself pointed out that it was structural issues and basically that any
Bolshevik in charge could have very well went down the same path and if
he or others did a military coup on Stalin it actually would have got
worse quicker.

So one last thing I want to really hammer in, due to AltHistoryHub's
point of divergence being as early as it is really any differences with
Trotsky's actual positions could be justified in that Scenario as I am
sure some people will point out, my issue is more we do know what
Trotsky called for in the 20s, even if we assume that say Trotsky being
more of an insider makes him less critical of things like the ban on
factions and aggressive in enforcing it I don't think it would justify
the differences in positions expressed in the civil war and economic
positions after. Hopefully this makes more sense once I get into it,
anyway this introduction is going on far far far far too long so let us
get into it.

Could Trotsky have really taken power?
======================================

To open I want to talk about say a few things I do agree with, the very
start of the video talking about how Trotsky really couldn't have taken
over in 1924, I think this is very much true. Trotsky was not skilled at
well being a politician, he was also known for being kind of an asshole
and not very diplomatic, in our time line this cost him allies at a few
points in the 20s. As well he very much seemed to have considered Stalin
stupid which I don't consider true, he really seemed to underestimate
his opponents. Moshe Lewin's Lenin's last struggle I think makes the
correct assessment and that no Trotsky was not capable of actually
taking power

>\"Trotsky alone would not have been capable of carrying out the
reorganization and consolidation and the preservation of those later to
be purged. Deutscher explains very well why he could not be Lenin's
\"heir\": when Lenin finally suc- cumbed to paralysis, for example, he
concluded the very kind of \"rotten compromise\" that Lenin had warned
him against.

>\...

>He succumbed to a fetishization of the Party, to a certain legalism and
to scruples that paralyzed him and prevented him from reacting
unhesitantly, as Lenin would have done, to what his enemies were doing
against him. As the founder, Lenin was not afraid of unmaking and
remaking what he had made with his own hands. He was not afraid of
organizing the people around him, of plotting, of fighting for the
victory of his line and of keeping the situation under control. Trotsky
was not such a man. Lenin disappeared and Stalin was assured of
victory.\"

Trotsky only joining the winning side?
======================================

Now onto the facts of the video and what I have issue with

>\"Trotsky was a Menshevik until 1917 when he realized they weren't going
to win out\"

So while Trotsky did side with the Mensheviks in the split initially, he
pretty much fell out with them by the end of 1904 and always existed
more floating in the middle ground between Menshevik and Bolshevik, his
main error in this time was that he was a \"liquidationist\" in that he
wanted both groups to merge. When he returned in 1917 he was a part of
the Inter-Borough Organization which really had no difference in
politics that would have excluded him from the
Bolsheviks[{% cite deutscher2003prophetarmed %}]

Trotsky did not jump ship because one faction was losing.

A brief background, in 1917 the Bolsheviks initially under control of
Alexander Shliapnikov opposed the provisional government. However the
return of Kamanev and Stalin from Siberia in the middle of March changed
this they took control of Pravda and began arguing in support of the
Provsional Government and argued against the slogan \"Down with the
war\", as well as they began pushing for unification with the
Mensheviks. Overall it took a much more conservative swing. Kamenev even
called Lenin's calls for revolution and smashing of the bourgeois state
to be \"anarchist ravings\" [{% cite rabinowitch1991prelude %} 45-49]

Because of this Lenin needed the aid of two figures that had often been
at odds with him previously, Nikolai Bukharin and Leon Trotsky, from
Cohen's biography of Bukharin.

>\"To make a socialist revolution, Lenin first had to radicalize his own
recalcitrant party, an uphill struggle that occupied him from April
until the final moment in October.

>He was able to do so in the end by bringing to bear his great persuasive
powers, but also by promoting and relying on people previously outside
the party's high command. Two groups were crucial in this respect: The
Trotskyists, who assumed high positions immediately upon entering the
party and played a major role in Petrograd; and the young left-wing
Bolsheviks, of whom Bukharin was the most prominent, who were especially
important in Moscow.\" [{% cite cohen1973bukharin %} 48]

It would be in May that Trotsky would be invited by Lenin to join the
Bolsheviks but his ego about his own group and becoming a Bolshevik got
in the way at least according to his biographer Isaac Deutcher, not
because of any position differences, there was also some issues within
his group not trusting the Bolsheviks so he did want to win them over
too. [{% cite deutscher2003prophetarmed %} 256-259]. At this point too the
Mensheviks looked more like \"the winning side\" they were in the
provisional government and were over twice the size of the Bolsheviks at
the All-Russian Congress of Soviets of Workers' and Soldiers' Deputies

I don't fundamentally disagree with the part after this, Trotsky was not
the skilled politician that Stalin was and was not very diplomatic, but
I covered this in the intro already.

The Testament and Lenin having no disagreements with Stalin, and Lenin's verbal support of Trotsky
==================================================================================================

First lets get the authenticity of the Testament out of the way. Kotkin
to my knowledge is literally the only major historian of this subject to
make any claims of it being less then authentic. Stalin himself never
claimed it was either. Kotkin even mentions his position is contrary to
\"entrenched scholarship\" Kotkin as well acknowledges that Lenin's
sister who was not an enemy of Stalin said it captured something of
Lenin's views.

The bigger issue I sort of take with is that there had been no other big
disagreements between Lenin and Stalin, as well as the idea of Lenin
supporting Trotsky vocally would have made a difference. Towards the end
of Lenin's life there was a few large disagreements where Lenin called
on Trotsky to defend his positions I will go over those below.

Monopoly on foreign trade
-------------------------

The Soviet State in 1921 had a monopoly on foreign trade, this meant the
state would handle all foreign trading as to prevent the internal
capitalists and market from directly interacting with external markets.
Figures like Bukharin, Sokolnikov and others opposed this monopoly and
wanted internal capitalists to be able to trade directly, they felt it
would just be bypassed by smugglers or the state would not be able to
take on these duties, Stalin supported ending it or at the very least
weakening it. Lenin felt ending this would destroy the national
industries and eventual soviet power. Lenin would successfully prevent
the removal of it in May of 1922[{% cite lewin2005lenin %} 36-37]

However 3 days after Lenin would become partly paralyzed and lose his
ability to speak, during this period moves would be made to remove the
monopoly on foreign trade this would be during October of 1922. Lenin
then begun the battle to undo this damage, first he made moves to meet
with Stalin and others to make sure it would reappear on the next
agenda. Then on October 11th he asked Trotsky to meet with him on this
problem, following that Lenin would send a letter to the Politburo
demanding the removal of the decision. They decided to put it up to a
vote of the Central Committee. Stalin would write a note on this letter
\"Comrade Lenin's letter has not made me change my mind as to the
correctness of the decisions of the plenum concerning external trade\"
though Stalin agreed to let the question be brought back up and for
Lenin to come and make an argument. However Lenin knew his health as in
a decline and he would not be able to defend it. In December Lenin asked
Trotsky that they should join forces and Trotsky agreed, however Trotsky
attempted to bring in a secondary issue about the gosplan and its powers
to regulate trade. Lenin wanted to put the second question off, but he
was ready to make concessions to Trotsky's position. \"At any rate, I
earnestly ask you to take upon yourself, at the coming plenum, the
defense of our common opinion.\" though December both men would
correspond with great length as well as other figures who shared Lenin
and Trotsky's opinion. December 15th Lenin wrote a letter to Stalin and
other members of the Central Committee that he had taken the steps to
retire, but he also declared \"I have also come to an agreement with
Trotsky on the defense of my views on the monopoly of foreign trade\" in
a postscript he said \"Trotsky will uphold my views as well as I\". On
the 18th the Central Committee annulled its previous decision. Lenin
would send another letter to Trotsky \"It seems we have captured the
position without firing a shot by mere movements of maneuver. I propose
we should not stop but continue the attack\" [{% cite lewin2005lenin %} 37-40]

It was actually this that caused an anti-trotsky alliance to form. Lenin
more vocally supporting Trotsky actually in some ways maybe have
worsened Trotsky's position as it unified his opponents.

Georgian Affair
---------------

Another area where Lenin took issues with Stalin near then end of his
life was the Georgian affair.

Following the Red Army Invasion of Georgia there was movement to
establish a Transcaucasion Federation to administer the region. Lenin
while encourage economic integration did support taking things slow and
concessions to the Georgian Mensheviks and permitting them in
government. The Transcaucuasion policy would run into issues with the
Georgian Communist Party's Central Committee through it was a bad idea
and things should be taken slower.[{% cite smithgeorgianaffair %} 521-530]

Stalin's autoonomisation project was that these republics would be
incorporated into the RSFSR as autonomous republics. Lenin would push
back on this plan and said that Russia, Ukraine and others would enter
as equals to Russia in the USSR. Georgia, Azerbaijan, and Armenia would
enter as equals, but as part of the Transcaucasian Federation. The
Georgian Central Committee was pissed and sent letters to Stalin,
Kamanev, Bukharin and Lenin about their opposition and they were
dismissed by each of them, this would result in the Georgian Central
Committee resigning and this would catch Lenin and others's attention.
Reports also came to Lenin of physical attacks and drawing of knives by
Bolsheviks towards fellow party members in Georgia. The exact reason we
don't know, though the information coming into him from Georgia was
probably a part of it. Lenin would a very sudden change of mood, and he
would declare the need to declare war on Great Russian chauvinism. Lenin
would come out for a weakening of the union and that the USSR would only
be a union for military and diplomatic affairs, the various republics
would be given power over their own affairs.[{% cite smithgeorgianaffair %}
530-535]

I am going to quote Jeremy Smith summarizing Lenin's notes on the
matter.

>\"Ordzhonikidze comes in for particular blame for the conflict in
Georgia. Dzerzhinsky also 'distinguished himself by his truly Russian
frame of mind' in whitewashing Ordzhonikidze. He and Stalin shared the
political responsibility for events in Georgia, and 'Stalin's haste and
his infatuation with pure administration, together with his spite
against the notorious \"nationalist-socialism\", played a fatal role
here'.\"

Dzerhinksky had been in charge of a commission to investigate this
behavior but Lenin was convinced he was covering things up and Lenin
became convinced that the Central Committee had a conspiracy to mislead
him.

Lenin would have his secretaries investigate and prepare their own
report, in it they would fully take the side of the Georgians, it was
highly critical of Dzerzhinsky and Ordzhonikidze, there however was no
criticisms of Stalin in the report. Though Lenin assumed he was a part
of the cover up in some way.

In March of 1923 he wrote to the Gerogians Mdivani and Makharadze 'I am
with you in this matter with all my heart. I am outraged at the rudeness
of Ordzhonikidze and the connivance of Stalin and Dzerzhinsky. I am
preparing for you notes and a speech'

But Lenin knew he was too sick and would not be able to defend the
Georgians and he turned to the only person he could trust to take up the
issue and defend them. He sent Trotsky a letter urging him to take up
the defense of the Georgians. However Trotsky would fail Lenin, in part
Trotsky was sick but he also failed to correctly see the importance of
the issue.

Stalin and other became aware of what Lenin had said to the Georgian and
he encouraged that compromise must be reached, Stalin was well aware at
this time it would be very bad for him at this time to be fighting both
Lenin and Trotsky.

Trotsky despite turning Lenin down did end up coming to the defense of
the Georgians to an extent and winning them some concessions and Stalin
accepted them. He even pushed to have Ordzhonikdize removed from his
post and 'deviationist' removed from the Georgians. But he only received
one vote in support which was probably Bukharin. All this made Trotsky
give up the fight. Eventually at the XII Congress Bukharin would make an
impassioned speech defending the Georgians it really ended with neither
side taking victory, and even had Trotsky fought Stalin more on it it is
unlikely even with him carrying out Lenin's wishes it would have been
enough to dislodge Stalin who was popular enough at the time combined
there was nothing directly implicating him in the affair beyond helping
cover it up.

Conclusion
----------

So to conclude on this section, It is possible even more support from
Lenin could have made a difference and there is more then what I showed
here, but if the above didn't matter idk how much more could have made a
difference. I just want to stress how improbable I think Trotsky taking
over would have been, and althistoryhub does mention his justification
is flimsy and he is not quite sure if it would have made a difference
and I kind of wanted to show why I think he is right it is flimsy.

Trotsky's influence from the Cypher guy
=======================================

Now onto the portion made by a person with historian in his channel name
so we can assume there will be some good research done for this part
\....

While it is true Trotsky held a lot of influence in the military this
actually hurt him in many ways not mentioned, this often made him the
most likely figure to betray the revolution in peoples eyes a Red
Napoleon.

So this whole bit I am actually not quite sure what their point is, they
list off a bunch of countries they think Trotsky invaded, and then go on
to talk about how he was extreme. Are they implying that Trotsky was
invading these countries against the wishes of the rest of the state and
so he was seen as extreme? I am not quite sure, but that is not how it
worked, Trotsky did not invade nations without approval from the
government. It is possible maybe they are just listing it off for
context that this occurred, but then why include Georgia and Finland?

I have an issue with the framing, when you list countries like this it
gives the impression that these are separate countries, but in the case
of the Russian Revolution many of these places had their own Red forces
who were on the side of the Soviets and local nationalist forces
sometimes on the side of the white army or fighting for themselves.

So in the context of the civil war I don't really think this is the same
thing as invading a fully separate nation with its own government, the
reason I say this is because the video seems to be using this part to
justify their alt-history Trotsky being a warmonger and that he was an
extremest within the party.

Finland
-------

One of the most confusing things they list is Finland, they list Trotsky
invaded Finland. I actually can't figure out where they get this idea
from, did they think the Winter War and Continuation War occurred during
the Russian Civil War? Do they think all of East Karelia is rightful
Finnish land and by not giving it to Finland it counts as an invasion?

Finland's Independence would be recognized by the Soviet Government in
1917, and other then sending some limited aid to the Reds in the Finnish
Civil war there was no invasion, and the aid was cut short due to
Germany demanding it stop. [{% cite carr1978bolshevik %}
188-289][{% cite mawdsley1987russian %} 117]

I could only find like 2 events that really was the nearest point of Red
Army and White Finland coming into conflict.

In May of 1919 the Fins pushed into East Karelia, but the British did
not support this move, and they retreated back across the border with
the approach of the Red Army [{% cite mawdsley1987russian %} 159]

In June of 1919 according to Red Army intelligence over 100k Finish
troops were building up near Petrograd and the White Army was trying to
get the assistance of Finland, but the whites wanted to reform the
Russian Empire, the Soviets offered them peace and a guarantee if they
stay neutral in the civil war. Mannerheim rejected the whites and took
up the Soviet offer for peace. [{% cite figes2014people %} 672]

Neither of these events could be considered an invasion at Finland at
all, so where they got the idea that Trotsky invaded Finland I have no
idea.

Georgia
-------

They also list Georgia as a country Trotsky invaded, rather then in the
case of the last one which was wholly an imagined invasion there was
actually an invasion of Menshevik Georgia.

In 1920 the Russian Soviet Republic concluded a treaty with Menshevik
Georgia recognizing its Independence. However in 1921 the Red Army
invaded and seized the country. Trotsky who was in the Urals at the time
of the invasion was enraged and when he returned to Moscow to he
demanded the creation of a commission to investigate the events
[{% cite smithgeorgianaffair %}]and \"bring to book the presumed adventurer\"
[{% cite deutscher2003prophetarmed %} 474] he would lose the vote, however.

Trotsky really had nothing to do with the invasion of Georgia in fact it
is well know it happened without his approval and he was pissed.

Poland
------

The situation with Poland requires a bit of history I think.

In early 1919 Poland made major pushes east, and due to the precarious
positions of the Soviet forces they made many concessions in terms of
land, in late November the politburo voted to accept any armistice with
Poland so as long as their campaigns against Petlyura in Ukraine.
Advances slowed down in part because Pilsudski was also not a big fan of
the whites and didn't want to hurt the reds so much that the white army
could pose a threat to Polish Independence. [{% cite carr1978bolshevikv3 %} 154]

There were negotiations of borders, but these would breakdown in spring
of 1920 when Poland would launch a major offensive and on May 6th would
take Kiev from the Red Army, though without any fighting as the Red Army
retreated. [{% cite figes2014people %} 698]

In July of 1920 Trotsky stated his intention with all Ukranian and
Belarusian territory secured he would order the red army to halt and not
advance any further and to make a public offer of peace with Poland.
Lenin and the majority of the Politbureau were for the continuation of
the war into Poland. No one in this fight argued for the idea that
Communism and Revolution be forced on an unwilling Polish population.
Lenin and others knew in 1917 there had been Soviets in Poland and very
strong support for Communism there, they believed them to still be there
and strong. Even in early 1920 Trotsky spoke about Polish Soviets, none
of them were fully aware to what extent they had been suppressed in
Poland in the years sense. The Politburo asked Polish Communists who had
joined the Bolsheviks and lived in Russia their opinions. Karl Radek,
and Felix Dzerzhinsky opposed the invasion and said it would result in a
surge in Polish patriotic sentiment. Another Polish Communist Lapinsky
greatly exaggerated the strength of Polish Communism. Trotksy would side
with the opinions of Polish Communists who opposed it. Trotsky would
submit to the decision of the majority and carry out his job despite
opposing it. When this war turned to disaster Trotsky argued in favor of
a peace deal, which Lenin would support him on.
[{% cite deutscher2003prophetarmed %} 464-469]

So Trotsky opposed the war, however he was outvoted and he carried out
his duty. Again this part of the video is supposed to be supporting
Trotsky's zealotry and why he was considered too radical by the party
standards, so how does that fit with him opposing the war?

Ukraine
-------

Onto Ukraine

Following the February Revolution a independent Ukrainian government was
setup in Kiev. They would seek recognition from the Provisional
government, not for full Independence, but just autonomy and certain
rights. The provisional government would reject their demands.
[{% cite smith2013red %} 20-21]

After the October Revolution there would be negotiations between the
Bolsehviks and the central Rada and some within the government actually
supported some kind of agreement with the soviet government like the one
with the provisional government. This would come to an end in December
and open hostilities would begin.

The Rada would fail to really gain any support to oppose the Soviet
forces, the idea of a Ukrainian nation was mostly exclusive to the
towns, peasants support was only won with the promise of land reforms.It
also lacked support in the east where the population was more Russian,
and among the industrial workers in the cities which were often Russian
and Jewish. While there was Soviets established in Ukraine, many either
didn't have Bolshevik majorities and opposed the revolution, or didn't
have majority support in their town. In Kharkov however the Bolsheviks
did have power and the Soviet there declared an Ukrainian Republic of
Soviets.[{% cite mawdsley1987russian %} 24]

At this time Trotsky was not in charge of the Red Army nor was there
really a proper red army yet. Antonov-Ovseenko was in charge and his
Chief of Staff was Mikhail Muravyov a Left-SR and he took about 1000 men
and went to take Kiev. As they moved closer a revolt would break out in
Kiev that would be crushed by the Rada, but his forces would end up
taking the city[{% cite mawdsley1987russian %} 24-25]

Now I actually can't find much information in any of my books on this
brief period of Soviet Ukraine under the control of Moravyov, I can't
find much descriptions on the events that I can fact check beyond Figgs
mentioning it was very brutal and another book mentioning it was
horribly Russian Chauvinist and oppressed Ukrainians, but it would be
short lived military run state due to Ukraine becoming German during the
Treaty of Brest-Litovsk

Eventually the German Ukranian state would end. The Rada reestablished
its self in Kiev, it would also claim Western Ukraine which caused
tensions with the Poles who also claimed it. Odessa at this time was
occupied by the French, and the Bolshevik Pyatakov formed a Ukrainian
Government in Kursk. A general strike in Kharkov would put the local
Soviet back in power. While the Red Army moved south, Chicherin at the
time said they were acting on their own to the Directorate of
Ukraine.[{% cite carr1978bolshevik %} 300-301]

Which may not have actually been a lie at least fully, Stalin may have
given authorization of the invasion of Ukraine without
approval.[{% cite figes2014people %} 706]

Trotsky's too wanted to push back into Ukraine the landing of french
troops worried him that even more would land, but Lenin was opposed.
[{% cite deutscher2003prophetarmed %} 428-429]

Now by early 1919 Ukraine was mostly back in Bolshevik hands. Pyatakov
was in control this time.[{% cite carr1978bolshevik %} 302-303] Pyatakov was one
of the clearest examples of Great Russian Chauvanism, in 1917 he was one
of the biggest opponents of self-determination for minorities within the
party, while being born in Ukraine he very much was Russian, he felt
separatists tendencies only served the bourgeoisie to stave off
revolution. [{% cite tucker1974stalin %} 170] So it is no surprise how he ruled in
Ukraine. Collective farms which failed and were not implemented in
Russia were forced on Ukranians, Ukrainian Nationalists were imprisoned
en-mass, this would result in peasant uprisings and a loss of control of
the Ukraine. Lenin would blame Piatakov personally for losing Ukraine to
the whites again due to his chauvinism towards Ukranians.
[{% cite figes2014people %} 706-707] The volunteer army and Petlyura would seize
back control of Ukraine and Kiev by September after a push north in
July. The brutality of these governments would make the brutality of
Pyatakov be forgotten. [{% cite carr1978bolshevik %} 302-303]

Denikin the leader of the Volunteer Army refused to acknowledge
Ukrainians even existed, they were just Russians to him. He wanted
Russian to be the official language of instruction in Ukraine.
Unshockingly this lost him the support of Ukranian nationalists.
[{% cite kenez1977civil %} 151-156]

But the worst crimes of Petlyura and Denikin is what would happen to the
Jewish population of Ukraine. Estimate of Jews killed in the pogroms
varies, but it was likely in the hundreds of thousands. If you count
those killed, raped, wounded and orphaned the number of victims is
around a million. This would have been during the volunteer armies short
period of control of mostly the only 6 months of 1919. While all armies
in the civil war pogroms committed progroms, none did it to this scale,
they were systematic. They would move and eliminate entire villages,
officers would declare so as long as there was Jews in Ukraine it could
not be secured.[{% cite kenez1977civil %} 166-172] It is no shock then that the
population turned on them

This time when the Red Army pushed back the Volunteer Army into Ukraine
and crushed it. Lenin stepped in in November of 1919 saying they must
find a common language with the Ukrainian peasants, he spoke out against
the primitive Russian chauvinism displayed by Bolsheviks. He called for
the use of Ukrainian language in all Soviet institutions. The Ukrainian
left-nationalists were admitted into the Ukrainian Bolshevik Party, it
was through accepting this nationalism and integrating it into the party
that peace would come to Ukraine finally. In the period following this
the Ukranian language would flourish, the Ukranian population of Kiev
would go from 27 to 42 per cent. [{% cite figes2014people %} 707-708]

During the First invasion was not Trotsky, the Second was mostly led by
Piatakov, and the Third was pushing back the volunteer army and not
invading any Ukrainian state in my view

Baltic States
-------------

Onto the Baltic states

In Estonia and Latvia both had Soviet Governments established at the
same time as the one in Petrograd. Remember the Bolsheviks were not just
Russians in European Russia they existed in all parts of the Russian
Empire. In the Baltic they were particularly strong. Though these
governments would be short lived in the Baltic and would either fall to
the British or Germans. [{% cite carr1978bolshevik %} 312-314]

### Estonia

In Estonia a white army push towards Petrograd would be supported, when
they were defeated they retreated into Estonia and Trotsky did in fact
call for an invasion to crush the white army, however Estonia realizing
the situation they were in and the Bolshevik diplomats were able to
offer Estonia a peace treaty if they stayed neutral, the white army in
Estonia would be disbanded and a peace treaty signed. [{% cite figes2014people %}
673-674]

### Latvia

Following the collapse of German controlled states both Nationalist and
Local Red forces fought in Latvia, there had been a very strong red
presence there and with the aid of units of the red army the Latvian SSR
was restablished in early 1919, though by May it had collapsed due to
economy destruction from the war, Germany connected forces and with the
Estonian Government pushing south and cutting it off from Russia and
sealing its fate. [{% cite mawdsley1987russian %} 118]

### Lithuania

This is a bit shorter as not really many of the books I have touch on it
that much. Parts of it were controlled by red forces in early 1919
briefly after Germany pulled back, but then it was lost to the White
army and Poland, in the intervening months Lithuania actually had a
government established an an army, but part of the country would end up
in Soviet hands again during the war with Poland, but due to the loss
against Poland any red forces left. [{% cite carr1978bolshevik %} 312-314]

### Conclusion

Now I can't find much on the Baltic States and Trotsky but 1 note at the
bottom of the page in The Prophet Armed that Trotsky had urged peace
with the Baltic states. So take that as you will.

Armenia
-------

Not a lot to write about here, at the time of the seizure of Armenia it
came after Armenia had already been defeated by Turkey, a local uprising
by local Bolsheviks followed by the Red Army had the government
surrender without a fight. Trotsky was not involved with this as he was
busy with Poland at the time. This was done under Lenin's orders.
[{% cite figes2014people %} 713]

Kazakhstan
----------

So to be honest for Kazakhstan and really anything within \"Russian
Turkestan\" the amount of information I can find is actually pretty low.

We come back to my general issue of talking about these things if they
existed as nations, by the point the Red Army really took Kazakhstan it
was just white army forces ruled over by Kolchalk, and when we talk of
Trotsky's invasion, he was in the west at the time Kamanev actually was
overseeing this front and Trotsky didn't want him to push east at that
time, and Trotsky got overridden, and Kamanev pushed east.

Azerbaijan
----------

We finally come to the last country listed.

From the time of its Independence to April 1920 it had 5 different
governments, the governments were blocked from land reform by the local
capitalists which pissed off the rural poor, and unemployment was high
due to no longer exporting oil to Russia, made a fertile ground for
Bolshevik recruitment among the lower classes. As well as the army
refused to resist and Baku was taken without armed resistance. Trotsky
at this time was busy with Poland and did not take part in the invasion.
[{% cite figes2014people %} 712]

So how does this show Trotsky's extremism
-----------------------------------------

None of these invasions that did happen were something he was for alone,
if he was even for them which he was not in several cases. This could
not have been something that made him an outsider to the party as there
was support from these and Stalin supported them as well so if that hurt
Trotsky wouldn't that also have hurt Stalin?

Military Opposition
-------------------

So ya, I don't see how all of that is supposed to show Trotsky was
disliked by being too extreme these weren't invasions done by Trotsky
alone, and in some cases there was no invasion or Trotsky was opposed to
it. However there was some disagreements with him on war policy, mostly
around his use of tsarists officials. As well as in general his military
experience I think hurt him because people viewed him as a potential
bonapartist, a military leader who might betray the revolution.

Trotsky and the Terror
----------------------

Here they are not wrong, Trotsky was one of the major defenders of the
Red Terror, and not all Bolsheviks supported the terror in the way it
was implemented, though very few were opposed to it on some level. So I
don't think this made him an extremest in the party given Lenin and
Stalin and others supported it. So again why would this mean \"no wonder
he was not well liked\" this was a popular position within the party,
how does supporting the terror make him an extremist that would make him
not liked by the vert party that implemented the terror?

I also find their definition of Terror to be inaccurate. They define it
as \"which was when the Bolshevik secret police suppressed decent
through terrorism and mass executions.\" I at the very least wouldn't
limit it to the actions of the Cheka, in many cases the terror started
well before the official as workers and peasants took revenge on those
they thought to be their enemies.

I also disagree with the comment about \"they werent called gulags yet\"
what existed during the civil war was very different from the gulag
system to quote from Soviet Penal Policy, 1917-1934: A Reinterpretation.

\"Civil war camps were located in the heartland of Russia, not in remote
regions of Siberia or the north. Some of their prisoners, as
Solzhenitsyn was aware, were allowed to live in residence outside of the
camps. More important, the food and clothing provided to camp inmate
were reportedly of relatively good quality, consisting in some
jurisdiction of one Red Army ration per inmate \... Nor can one speak of
a direct evolution from the civil war camps to the Stalin camps, because
the former were closed in 1922 after the penal systems of Narkomiust and
Narkomvnudel merged. The immediate forebears of the Stalinist camps were
the northern camps of the OGPU in Solovki, which were opened in 1921-22
and allowed to coexist with the new administration.\"

The bulk of the prisons/camps in the NEP era never resembled the gulag
system. This is something I would like to do a future video on, how the
rather progressive for the time system in use during the NEP died and
was replaced with the GULAG. [@SolomonSovietPenalPolicy]

As well Trotsky's red army made use of POW labor which is kind of a
reality of war, during WW2 here in Kansas we used a lot of German POWs
as agricultural laborers. The Geneva Convention which did not exist at
the time even permits for POWs as laborers. I don't think use of POWs as
labor is comparable to the GULAGs as well.

Trotsky in Charge Now
=====================

We are now onto the part of the video where Trotsky is in charge, this
is less history based and more speculation and so I can't really say for
100 percent any given thing here is wrong, but I still want to point out
there things don't line up with previous positions of Trotsky. I
understand it is alt-history and at the end of the day you could say
Trotsky goes mad and starts to eat children and well it is ok because it
is alt-history.

Trotsky's Use of Experts
------------------------

I don't know if I would say it is his main criticism, but yes Trotsky
was always someone for consulting experts and he had a dislike of
letting party officials take care of things, this can be seen during the
civil war with him using Tsarist officers, or after where he wanted
experts in control of economic choices and not the party. So ya
alt-history hub I think is accurate in this.

Do want to mention/show that Trotsky's use of experts didn't mean he was
for some detached technocrats running everything. This is from Trotsky
in 1925.

>\"We must not build socialism by the bureaucratic road, we must not
create a socialist society by administrative order .. Socialist
construction is possible only with the growth of genuine revolutionary
democracy\" [{% cite RichardBDay2004leon %} 142]

Gulags
------

Then he brings up gulags and I think I responded to this kind of
earlier, to my knowledge I can't ever find Trotsky commenting much on
this, though the major change in Soviet Penal policy happened after his
exile so it is hard to know what his position on it might have been. A
big part of this seems to be based on Trotsky as a \"fanatic\" which I
am not quite sure where the argument for is from.

Holodomor and the Kulaks
------------------------

I have some major issues with this part, yes Trotsky was for
collectivization, but his plan differed vastly from Stalin's I actually
did a video on this.

I don't think the holodomor would have occurred under Trotsky, he
generally had a better understanding of agriculture given his background
growing up on a farm in Ukraine. The spiral towards the grain strike was
Stalin's main motivating factor in him losing confidence in the NEP and
switching to forced collectivization.

To quote Richard B. Day

>\"The only policy that might have avoided the \"grain strike\" was put
forth by Trotsky. Underlining the dangers inherent in the goods famine,
Trotsky consistently appealed for accelerated investments in the
consumer goods industries.\"[{% cite richard1982selected %} liv-lv]

As I covered in my video Did Stalin \"Steal\" Trotsky's Economic
Program? Trotsky was not for forced collectivization, he was not for
violently ending the Kulaks. In 1923 he condemned the idea of
de-kulakizing proposed by Zinoviev.

To use a bit of the quotes I used in my other video which you should go
watch in full.

>\"Trotsky's actual economic proposals in the 1920s were based on the NEP
and its continuation. He urged greater attention to heavy industry and
planning earlier than did Bukharin, and he worried more about the
village \"kulak\"; but his remedies were moderate, market-orientated,
or, as the expression went, \"nepist.\" Like Bukharin, he was a
\"reformist\" in economic policy, looking towards the evolution of NEP
Russia towards industrialism and socialism.\" [% cite tucker1977stalinism %}]

>\"In propaganda texts, the majority's spokesmen accused the Left of
planning to liquidate the NEP, to oppress the peasantry, to raise prices
and lower the standard of living, and other sins. But the latter, no
doubt sincerely, reasserted that it favored the NEP, did not intend to
expropriate the property of the kulaks, nor indeed, that of any other
private entrepreneurs, and that it, in fact even, welcomed some growth
of these elements provided the growth of the socialist sector, mainly
industrial, was constantly assured. They opposed using the G.P.U.
against the private sectors.\" [{% cite lewin1974political %} 35]

### Trotsky on Trading with Capitalist Nations

They say Trotsky was not for trade with capitalist nations, I have no
idea where they got this fact from, Trotsky very much was for. Trotsky
was for buying foreign consumer goods to help make sure the peasants
were happy in areas where local soviet industry was lacking to give them
time to build up those industries [{% cite RichardBDay2004leon %} 140-141]

Trotsky even supported grain exports, he wanted to import foreign
industry to help with industrialization.[{% cite richard1982selected %} 85]

Trotsky had been against certain plans, and wanted to make sure there
was a monopoly on foreign trade, but he was not against foreign loans
and trade as he thought it was the only way to break the deadlock
between agriculture and industry.

He Agreed with the Goals but not the methods
--------------------------------------------

My single biggest issue with this video is how they talk about it being
a minor thing that Trotsky disagreed with the methods? But methods are a
huge thing, if I hate that my neighbor lets his dog poop on my lawn and
I want to him to stop, and my method is talking to him or putting up a
fence, but my roommate decides to murder my neighbor and his dog. Sure
we had the same goals, but we disagreed on the methods. I don't know how
someone can act like having a difference in opinion on methods is a
minor aspect.

Trotsky In charge and WW2
=========================

I am mostly going to skip over this part, on what Trotsky in charge WW2
would look like is not an interesting question to me and I don't have
much to add, though I think they paint Trotsky was a warmonger, and well
I made a video on that you should go watch it if you want my thoughts on
that. I also don't want to get into the whole Socialism in One country
thing, I think it is the incorrect way to examine the differences to
Trotsky and Stalin.

Bad Military Choices Poland 
----------------------------

They claim at around 12 minutes that Trotsky didn't make the smartest
military choices because he invaded Poland. Trotsky opposed the invasion
of Poland.[{% cite deutscher2003prophetarmed %} 464-469]

So I don't know how this can show he made bad choices when he was
opposed to it.

Trotsky's Legacy
================

I am not going to go in depth on this part, a lot of their assessment
here is on faulty ground as has been shown. I take issue with them
acting as if Trotsky is partly responsible for this what if scenario as
I showed in the opening he shot that down. Also no Trotskyism is not
based on the idea of what if Trotsky was in charge. I also disagree with
a core idea of theirs in that Trotsky did some brutal things during the
civil war therefore he would have taken the same actions as Stalin,
there is a difference between actions in war and peace time, so I don't
think it is an absolute guarantee.

Trotskyist's created Neo-Conservatism
-------------------------------------

This is probably the single wrongest idea expressed in the video. I am
actually so tired of this myth I am going to do a whole video on it. I
will give a short overview here. I am just going to quote William F.
King's Neoconservatives and \"Trotskyism\" which was published in
American Communist History, in 2004.

\"Yet today, as a result of a civil war within American conservatism, it
is precisely the history of \"the neocons\" that is being distorted
through a polemical campaign aimed at prominent neoconservatives and the
foreign policy of the Bush administration. Leading the campaign against
the neocons are the self-styled paleoconservatives, an intellectual
faction made up of liber- tarians, right-wing populists, and
traditionalist conservatives who consider themselves the legitimate
successors to the pre-Cold War Old Right. In an attempt to discredit the
neocons' conservative credentials, the \"paleocons\" have forcefully
asserted that neoconservatism is a descendant of American Trotskyism,
and that neoconservatives continue to be influenced by Leon Trotsky in
their views on foreign policy. Refiecting a propensity for flirting
dangerously with when not openly embracing anti-semitism,
paleoconservatives have even charged that a \"cabal of Jewish neocons\"
is manipulating US foreign policy and implementing Trotsky's theory of
permanent revolution from the White House.\"

\"Very few (four) of the original neoconservatives were ever
Trotskyists. The small minority of neocons that were involved with the
movement passed briefiy and marginally through it during their late
adolescence.\"

This is basically just old slander and anti-semitic conspiracy. Now not
everyone who believes in this is anti-semitic or is arguing there is a
Jewish Cabal controlling modern American conservatives. But some of the
people who helped initially popularize the theory of neo-cons descending
from Trotksyism without a doubt did think there was a Jewish cabal.

Conclusion
==========

I don't have much to add now in the conclusion, at the end of the day
alt-history is entertainment. The core issue is and what promoted me
making this video is seeing people sharing this in more serious
political spaces or repeating things they heard from it. It is a fun
enough little video as long as you don't take anything said in it as
historical fact, even the part trying to cover history by the person
with \"historian\" in their channel name.

As with all of my videos you can find the script in the description
which contains my work cited. To end I want to just repeat the sentiment
that Trotsky himself mocked the idea that just replacing Stalin with him
would change the USSR into a bulwark of Socialism, and compared it to
putting a socialist in charge of the catholic church.

Hope you found this video informative, please go watch the other two
videos of mine I refereed to in this video \"Was Leon Trotsky for
Spreading the Revolution via the Red Army, and Did Stalin \"Steal\"
Trotsky's Economic Program?\"



# References

{% bibliography --cited %}
