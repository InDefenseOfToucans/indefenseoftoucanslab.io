---
layout: post
author:
- InDefenseOfToucans
bibliography:
- sources.bib
date: Jul 13 2020
title: Second International and Immigration
---

<iframe width= "100%" height= "400px" sandbox="allow-same-origin allow-scripts allow-popups" src="https://watch.breadtube.tv/videos/embed/54e828b5-a1f3-49b6-ae67-c2a9b3198928" frameborder="0" allowfullscreen></iframe>

[Click here for the PDF version of this script]({{ site.url }}/assets/scripts/secondinternationalimmigration.pdf)



Introduction
============

I see people today claim that caring about issues such as immigration is
some distortion by the modern left that has abandoned class issues. This
position I think comes from general historical illiteracy, the left has
always considered immigration an important issue. Even among the
chauvinistic Second International it was considered important and very
pro-immigration views were the norm. This is going to be a bit different
from my other videos, I am going to give a breakdown of some of the
people involved in this debate in the second international, but the main
body of this is going to be a exchange between delegates at a Congress
in 1907 discussing the issue.

The Delegates
=============

Diving in lets take a look at some of the delegates. Apologies ahead of
time for my inability to pronounce things.

Manuel Ugarte was a represenative from the Argentinian Socialist Party,
when WW1 broke out he took an anti-imperialist position against the war
for this he was removed from the party and drifted away from Socialism.

Morris Hillquit who would be a delegate from the American Socialist
Party. He was born in Riga, Latvia to a family of German speaking Jews.
He went to a Russian school due to the limitations on Jews in the
Russian Empire meant he was not able to attend a German school. In 1886
he would emigrate to New York. In 1887 the following year he would join
the Socialist labor Party, and become active in its Jewish section,
helped setup Newspapers appealing to Jewish immigrants and and
publishing things in Yiddish. Though he expressed he would have
preferred to organize amongst \"americans\" and disliked the ethnic
separation with the SLP. Eventually Hillquit would fight with Daniel De
Leon and formed an anti-deleon faction and eventually split and that
split would join with Victor Berger and Eugene V Debs to form the
Socialist Party of America, while in the Socialist Party he ran for
Mayor of New York and for congress several times unsuccessfully, and was
generally considered part of the parties right wing. [{% cite pratt1979morris %}
1-93]

Hillquit I find an actually very interesting person, don't agree with
him, but if people want it I might do a longer video on him, let me know
in the comments.

Charles Rappaport Was born to a Jewish Family in Tsarist Russia. He was
active in the Russian revolutionary movement before being forced into
exile, he would become naturalized in France and contribute to many
socialist journals. He was active in the Second International and was an
opponent of WW1, while initially critical of the Bolsheviks he would
join the French Communist Party and the Third International he
eventually would resign in 1938 in response to the trial of Bukharin.

József Diner-Dénes would be the representative from Hungary, was a
prominent Hungarian Social Democrat, eventually became part of the
Hungarian government until the dictatorship

Wilhelm Ellenbogen Founding member and delegate from the Austrian Social
Democratic Party, pacifist during WW1, joined the Social Democratic
government in Austria after the war, eventually was forced to flee
Austria during the rise of fascism.

Tojiro Kato A representative of the short lived Japan Socialist Party
which existed for a brief period between 1906-1908, it fell apart and
shortly after Japanese Socialism would be pretty much destroyed
following the High Treason Incident which was an plot to the assassinate
the Japanese Emperor, this caused a brutal crackdown on leftists in
Japan. After that occurred he went onto run a clinic for the poor.

Julius Hammer a representative from the Socialist Labor Party, I
actually had a hard time finding out a ton on him, most information
comes from short descriptions in articles and things about his son who
is a very interesting person. Julius Hammer was a Russian Jew who lived
in Odessa before immigrating to the USA and living in the Bronx where he
ran a medical practice. He would later go on to help found the Communist
Party USA, in 1921 he would be arrested for performing an abortion.

Background of the Debate
========================

Now you know a bit about the delegates who's speeches we will be reading
a portion of, but I feel we need to cover a bit of backstory of exactly
why this occurred and it starts within American Socialism.

At this time in American Socialism there was two main parties the
Socialist Labor Party with Daniel De Leon as its leader, and what
started as a dissident faction from the SLP led by Morris Hillquit and
merged with Victor Berger's and Eugene V. Deb's Social Democratic Party
to form the Socialist Party. Also remember at this time Social Democrat
meant Marxist, Lenin regarded himself as a Social Democrat, so don't
attach our modern understandings of Social Democracy. Now Victor Berger
is someone we will talk about later specifically his response to this
congress, and I would hope most of you know who Debs is.

During the oughts of the 1900s there was growing desire to get the
American Federation of Labour on the side of the Socialist Party and
voting for them, but the American Trade Union movement was extremely
racist and a major issue for them was specifically Asian immigration. To
win the support of the leaders of the AFL would mean promoting a
restriction of immigration from Asia, but this was an issue for the
Socialist Party as the Second International broadly was pro-immigration
and against restrictions on it. There was to be a congress in 1904 and
both Morris Hillquit and Daniel De Leon acted as representatives at the
congress. Hillquit along side delegates from Holland and Australia
submitted a resolution saying they would support their governments
attempt to exclude people of \"backwards races\" [{% cite pratt1979morris %} 93]

Now Wikipedia claims to have the text on this resolution as you can see
here, you may pause to read it. However it lacks a citation for it, and
it seems to imply Daniel De Leon was the author of sorts. Every source I
have says he was pro-immigration and there is evidence he was, not to
say he was not racist, he opposed combating lynching as that was
\"petite bourgeois issue\", but he did not support this resolution. The
Socialist Labour Party under De Leon was actually attacked by the
Socialist Party as being an immigrant party so I don't think he had a
part in drafting this and Wikipedia is misrepresenting his views. My
best guess is someone got it from a book that has it that is quoting De
Leon reporting on the resolution and it is him quoting it but I don't
know for sure.

I actually can't confirm the language of the resolution, the books I
have that cover it while mentioning a few lines from it all the
citations result in a dead end at just another summary.

The 1904 congress decided to shelve the issue until it could be brought
up more fully at the next congress. In the intervening years the
Japanese Socialist Party became aware of the attacks on Japanese and
Chinese in America and then sent a letter calling on them to take a
stronger position on this.

Even though I would hope most of you are aware to some extent to
horrible conditions that Asian immigrants were under at this time I want
to provide a brief overview of the laws and things and the conditions of
Asian immigrants at this time, first in 1875 Congress trying to bypass a
treaty banned people where were coming to work in the US and women who
were migrating to become prostitutes, of course the way this was
implemented it ended being really a ban on women all together, and later
law would fully ban women. The goal of banning women was so this
population would die out, this was not the only time the US did this
Women from Philippines were also banned. It was also at this time
impossible for a Chinese person to become a naturalized in the US. There
was also lynchings of Chinese and other Asian migrants. For example one
of the worst mass lynchings was in 1871 in Los Angeles. In 1882 the
Chinese Exclusion Act bared most Chinese immigrants. Initially supposed
to be temporary it was renewed in 1892 and 1902, it would be partly
repealed in 1943, but it was just replaced with a system that restricted
Chinese immigration to essentially nothing and that would not be
abolished until 1965. I am providing a brief overview and not really
getting into the depth of this horrible racist policy, you should do
some further reading on this.

All of this was just a start to a broad opposition and targeting of
Asian immigrants in general, in the period following the Chinese
Exclusion Act organizations like Asiatic Exclusion League would be
formed, the Knights of Labor also ramped up with anti-Asian sentiments.
They directly participated in violent acts on immigrants. Like the 1885
expulsion of Chinese people from Tacoma Washington, as well as the Rock
Spring's Massacre which resulted in the death of 28-50 Chinese Miners
carried out by White miners who were mostly involved with the Knights of
Labor. The excursionist movements argued that Japanese immigrants as
being untrustworthy without morals as well as scare about \"race
mixing\". After 1905 the Russo-Japanese War much of this racist
propaganda started to focus on the idea of invading \"Asian Hordes\" as
part of aid of an eventual Japanese invasion of the west coast. This
sort of dual loyalty thing has been applied to many groups in many
countries, one modern example of this sort of rhetoric can be seen today
with Chinese students being seen as agents of China and there being
calls to ban them.

So It is important to know the American Labour movement was very racist
and either in the case of some groups directly led the massacres and
lynchings, or simply ignored it and kept their unions white only.

This was the background that caused the Japanese Socialist Party to
write a letter to American Socialists basically asking them to quit
being so racist and do something about this and come to the aid of the
Japanese and Chinese Workers. This was published in The Socialist Party
Official Bulletin Volume 3 January 1907. I am not going to read the
whole thing to keep this thing short, but a key party.

>\"The Socialist papers of America have not been quite clear in their
general attitude toward the Japanese expulsion question, and comrades of
Japan are asking whether or not American Socialists are going to be true
to the exhortation of Marx \"Workingmen of all countries unite!\" or
whether they are encouraged contention and division on the ground of
race prejudice

>\...

>We believe that the expulsion question of the Japanese laborers in
California is much due to racial prejudice. The Japanese Socialist
Party, therefore hopes that the American Socialist Party with endeavor
to bring the question to a satisfactory issue, according to the spirit
of international unity among workingmen. We also ask the American
Socialist Party to acquaint us with its opinions as to this question\"

This would go unanswered. In this same issue I think I might have found
a translated reprint of the amendment proposed at the 1904, the issue is
the proceeding paragraphs explaining what it is in full is damaged on
the scan. It is part of a call from Hillquit for the party to draft a
resolution for the 1907 Congress as issues of immigration will be talked
about there. [{% cite kipnis1952american %} 276-277] In March the National
Executive Committee adopted it and following that in April the National
Committee of the Socialist Party adopted it with 46 voting yes and only
3 voting no and 11 abstained.

I am going to read the demands portion of their resolution they were to
submit to the Stuttgart Congress This can be found in The April 1907
edition of The Socialist Party Official Bulletin.

>\"Fully recognizing the above consideration, the Congress, therefor
declares it to be the duty of the Socialists and organized workingmen of
all countries: 1. To advise and assist bonafide workingmen immigrants in
their first struggles on the new soil: to educate them to the principles
of Socialism and trade unionism: to receive them in their respective
organizations and to collect them in the labor movement of the country
of their adoption as speedily as possible. 2. To counteract the efforts
of misleading representations of capitalist promoters by publication and
wide circulation of truthful reports of the labor conditions of their
respective counties especially through the medium of the international
bureau. 3. To combat with all means at their command the willful
importation of cheap foreign labor calculated to destroy labour
organizations, to lower the standard of living of the working class, and
to retard the ultimate realization of Socialism.

>The Congress class upon the Socialist representatives in the Parliaments
of the various countries to introduce legislation along the general
lines laid down in the resolution, as well as legislation tending to
secure immigrated workingmen full civil and political rights int he
countries of their adoption as speedily as possible. The Congress leaves
it to the various national organizations to apply the principles herein
announced to the specific needs and conditions of their respective
countries\"

Gone was the language of \"backwards races\", now don't think this is
because the American Socialists had a change of heart, I think it is
obvious that this was very well thought out to be as palpable to the
international congress after the very negative reaction they got to the
1904 proposal. But this was the resolution unalterned to my knowledge
they submitted to the 1907 Stuttgart Congress. Now let us go look at
some of the debate on this at the 1907 congress. I am pulling the bits
from John Riddel's Lenin's Struggle for a Revolutionary International.

The Debate
==========

Manuel Ugarte a representative from Argentina

>\"We Argentine comrades raised the question of immigration and
emigration at this congress for the following reasons. We want to combat
only artificial immigration: that is, immigration carried out by the
capitalist government agencies to obtain cheap labor to compete with
organized workers. Our comrades also demand measures against the
shipping companies' exploitation of emigrants.This is not a racial
question, and the resolution is not anti-Chinese or anti-Japanese.
Argentina should be open to all workers. But workers should be advised
of the working and living conditions of any countries which they wish to
emigrate. The Argentina comrades are proposing two resolutions to this
end. One demands that emigrating workers be informed about the
conditions of work, the other demands that the progress of
naturalization in the different countries be made easier so that workers
can immediately acquire political rights in their new place of
residence\....\"

Morris Hillquit a representative for the United States

>\"Immigration and emigration pose a very difficult, serious problem. Our
resolution in no way infringes on the principle of internationalism,
which has always been our guide in the United States. there are several
kinds of immigration; the first is natural immigration, which arises
from the very nature of the capitalist economy. For these immigrants we
demand full freedom, and we consider it the workers' duty to assist the
poor among them. Another kind of immigration must be sharply
distinguished from the first. Basically it amounts to capitalism's
importation of foreign labor cheaper than that of native-born workers.
This threatens the native-born with dangerous competition and usually
provides a pool of unconscious strikebreakers. Chinese and Japanese
workers play that role today, as does the yellow race in general. While
we have absolutely no racial prejudices against the Chinese we must
frankly tell you that they can not be organized. Only a people well
advanced in its historical development, such as the Belgians and Italian
in France, can be organized for the class struggle. The Chinese have
lagged too far behind to be organized. Socialism is by no means
sentimentalism. A fierce struggle rages between capital and labor, and
those who stand against organized labor are our enemies. Do we want to
grand privileges to foreign strikebreakers, when they are locked in a
struggle with native-born workers? If we fail to take measures against
the importation of Chinese strikebreakers, we will thrust the Socialist
workers' movement backwards. While the french resolution undermines the
principle of class struggle, ours holds it high. WE do not insist on its
every word, but we hope you will adopt a resolution with its general
approach.\"

József Diner-Dénes representative for Hungary

>\"Those countries that cannot be organized today will be organized
tomorrow. Moreover in backward countries this evolution proceeds more
rapidly than it did in countries that developed earlier, such as England
and Germany. Only ten years ago our Hungarian workers emigrating to
America were considered unorganizable. Today, only a few years later,
they are being organized and are inspired with the spirit of socialism.
You want to erect protective barrier around the workers. This will land
you in the same fiasco as have tariff-building efforts of capitalists.
If the wage question were merely one of supply of demand, we would have
to oppose the importation of agricultural machinery, since it had
replaced more workers than the Japanese and Chinese, especially in the
Eastern European countries. We must permit completely free immigration
and emigration. A great many American workers are wage conscious and but
not yet imbued with a proletariat class consciousness. Of course we must
fight against the abuses that stem from the mass importation of workers
for the capitalist' benefit, but through explanation and organization. A
good method would be the press the establishment of a minimum wage where
possible through political means, otherwise through trade union
struggle.\"

Charles Rappaport a representative from France

>\"We cannot accept Hillquit's talk of predestined strikebreakers. So
long as a worker has not acted as a strikebreaker, we treat him as a
comrade. We too want to take a stand against immigration organized by
the capitalists to break contracts, but not by fighting against the
workers involved.\"

Wilhelm Ellenbogen a representative from Austria

>\"The discussion is moving in two opposed directions. Some speak for the
interests of the country of immigration and others for those of the
emigrants. No reconciliation appears possible between the two points of
view\.... But we must combine them and make provisions for both sides.
This is best done by excluding from the outset measures unacceptable to
Socialists, such as guild like and discriminatory laws. I hope Comrade
Hillquit will not be offended, but I can not accept his resolution
because it is not clearly formulated. We should avoid distinction such
as those between \"natural and \"unnatural\" immigration which are
slippery and hard to define. However, we do have a number of positive
measures, in which the main tasks fall to the trade unions. The unions
should reach out to the countries of emigration and educate the
emigrants there, as the German trade unions have done in such exemplary
fashion. They must also try to prevent the export of strikebreakers.
Most important, the trade unions of the country of immigration must make
special efforts to attract the immigrant workers. Here I find it most
regrettable that many American trade unions make it difficult for
immigrants to join. Social legislation poses a second set of tasks. The
proposal of Diner-Dénes to demand a minimum wage should be supplemented
with one for a limit on the hours of work. WE must also demand
supervision of recruitment, and above all, regulation of conditions on
the emigration ships. A requirement of certain air space per person in
the cabins would make Chinese immigration in its worst for impossible,
since their transportation would no longer produce a profit\....\"

Tokijiro Kato a representative from Japan

>\"As the representative of the Japanese Socialists, I must take the
floor on this very important question. When the Americans exclude us
from California they gave us two reason: first, that Japanese workers
were depressing the wages and living standards of the indigenous
workers, and second, that we were taking away their opportunity to work.
I disagree with this. Not only the Japanese but also the Italians,
Slovaks, Jews, and so forth do this. So why is it only the Japanese are
being excluded? The race question obviously plays a role here, and the
Americans are clearly being influenced by the so-called yellow peril.
The history of the United States confirms this opinion. Another factor
is that the American capitalists want to flatter their workers. The
Japanese are under the heel of capitalism just as much as are other
peoples. It is only dire need that drives them from their homeland to
earn their livelihood in a foreign land. It is the duty of Socialists to
welcome these poor brothers, to defend them, and together with them to
fight capitalism. The founders of socialism, above all Karl Marx, did
not address themselves to individual countries but to all humanity.
International is inscribed on our banner. It would be a slap in the face
to socialism if you were to exclude the poor, exploited Japanese.\"

Julius Hammer representative from the United States Socialist Labor
party

>\"There is no middle course in this question of immigration and
emigration. Either you support restriction of immigration, or
energetically combat it. Hillquit's resolution is an attempt at
compromise that misses the mark. I especially oppose its third point
that envisages possible restrictions on the immigration of Chinese and
Japanese workers. This is completely anti-socialist. Legal restriction
of immigration must be rejected. Nothing can be gained for socialism
thorough legislative action, or through collaboration with the bourgeois
parties.(The speaker cites several examples of how racial hatred in
American blinds the workers and drives them to acts of violence) The
Japanese and Chinese could be very effectively organized. They are not
as unskilled as you might suppose. They are becoming quite well
acquainted with capitalism and are learning how to fight it. I ask that
you not approve any legal restrictions on immigration and emigration. We
must create a great nation of the exploited.\"

Outcome
=======

The Congress would not adopt Hillquit/Socialist Party's resolution,
though they would not fully endorse the Socialist Labor Party's position
of 0 legal restrictions on immigration which is about the same as the
modern slogan of open borders. The bits in the resolution on minimum
wage would be removed by Karl Kautsky and Rosa Luxemburg.

Now I was able to find the resolution in German, but after some
searching I found The Weekly Worker of the Communist Party of Great
Britain actually published an English translation of it online. I will
include a link to this in the description if you want to read it and see
their article on it.

But here is what the Congress adopted on this question, and I am going
to read it in full because I haven given up on the idea of making short
videos.

>\"The congress declares:

>The immigration and emigration of workers are phenomena that are just as
inseparable from the essence of capitalism as unemployment,
overproduction and workers' underconsumption. They are often a way of
reducing the workers' participation in the production process and on
occasion assume abnormal proportions as a result of political, religious
and national persecution.

>The congress does not seek a remedy to the potentially impending
consequences for the workers from immigration and emigration in any
economic or political exclusionary rules, because these are fruitless
and reactionary by nature. This is particularly true of a restriction on
the movement and the exclusion of foreign nationalities or races.

>Instead, the congress declares it to be the duty of organised labour to
resist the depression of its living standards that often occurs in the
wake of the mass import of unorganised labour. In addition the congress
declares it to be the duty of organised labour to prevent the import and
export of strike-breakers. The congress recognises the difficulties
which in many cases fall upon the proletariat in a country that is at a
higher stage of capitalist development, as a result of the mass
immigration of unorganised workers accustomed to lower living standards
and from countries with a predominantly agrarian and agricultural
culture, as well as the dangers that arise for it as a result of a
specific form of immigration. However, congress does not believe that
preventing particular nations or races from immigrating - something that
is also reprehensible from the point of view of proletarian solidarity -
is a suitable means of fighting these problems. It therefore recommends
the following measures: I. For the country of immigration

>1\. A ban on the export and import of those workers who have agreed on a
contract that deprives them of the free disposal over their labour-power
and wages.

>2\. Statutory protection of workers by shortening the working day,
introducing a minimum wage rate, abolishing the sweat system and
regulating home working

>3\. Abolition of all restrictions which prevent certain nationalities or
races from staying in a country or which exclude them from the social,
political and economic rights of the natives or impede them in
exercising those rights. Extensive measures to facilitate
naturalisation.

>4\. In so doing, the following principles should generally apply in the
trade unions of all countries:

>\(a\) unrestricted access of immigrant workers to the trade unions of
all countries

>\(b\) facilitating access by setting reasonable admission fees

>\(c\) the ability to change from the trade union of one country to
another for free, upon the fulfilment of all liabilities in the previous
union

>\(d\) striving to establish an international trade union cartel, which
will make it possible to implement these principles and needs
internationally.

>5\. Support for trade union organisations in those countries from which
immigration primarily stems. II. For the country of origin

1\. The liveliest trade union agitation.

>2\. Education of the workers and the public on the true state of the
working conditions in the country of origin.

>3\. An active agreement of the trade unions with the unions in the
country of immigration for the purpose of a common approach towards the
matter of immigration and emigration.

>4\. Since the emigration of labour is often artificially stimulated by
railway and steamship companies, by land speculators and other bogus
outfits, and by issuing false and scurrilous promises to the workers,
the congress demands:

>l The monitoring of the shipping agencies, the emigration bureaus, and
potentially legal or administrative measures against them to prevent
emigration being abused in the interests of such capitalist enterprises.

>III

>Reorganisation of the transport sector, especially ships; the
appointment of inspectors with disciplinary powers, recruited from the
ranks of unionised workers in the country of origin and the country of
immigration, to oversee regulations; welfare for newly arrived
immigrants, so that they do not fall prey to exploitation by the
parasites of capital from the outset.

>Since the transport of migrants can only be statutorily regulated on an
international level, the congress commissions the International
Socialist Bureau2 to develop proposals to reorganise these matters, in
which the furnishings and the equipment of ships must be standardised,
as well as the minimum amount of airspace for every migrant. Particular
emphasis should be placed on individual migrants arranging their passage
directly with the company, without the intervention of any intermediate
contractor.

>These proposals shall be passed on to the party leaderships for the
purposes of legislative application and for propaganda"

Now as far as international reactions Lenin and the Bolsheviks as well
as Karl Liebknecht would release statements in support of it. Lenin
characterized the struggle as between opportunists and revolutionaries.
Most parties were fully in support of the actions of the congress. The
future communist party of America in a report at a Comintern congress
would make explicit the need to organize and stand with immigrant
workers, this was considered a key part of the national question.

Reaction in America
===================

However the reaction in America would not be so positive, the parties
right wing represented by Victor Berger who I mentioned earlier, he
denounced Hillquit as being intellectuals who had betrayed the American
proletariat that would permit Japanese and Chinese into the country,
though he used less nice language, and declared that Socialism in the US
and Canada that they must remain a White mans country. Ernest Untermann
of the center faction said that after the class struggle had ended it
would become a global race struggle and it would determine what race
would rule the world. The parties left wing as well said that racial
incompatibility was a fact.

At a National Executive Committee meeting Victor Berger warned there
would be 5 million \"yellow men\" invading per year, and that the US
already had one race question and if something was not done the US would
become a \"black and yellow country\". The meeting also declared the
international had no power over the American Socialists, and that at the
time the party must stand in opposition to Asiatic immigration.

In 1908 at a party convention a full scale debate would occur and the
convention would adopt a middle ground declaration and say the race
question should be saved for the next convention. In 1910 Hillquit at
this congress too would be attacked as being too progressive, because he
fought against a resolution explicitly codifying race into it. Victor
Berger continued to be racist, and declare he would fight to defend
civilization from Asian immigrants. The congress also moved to call for
bans for anyone of a \"mongoloid\" race [{% cite kipnis1952american %} 277-287]

Also I want to point out Victor Berger's Wikipedia page makes no mention
of how racist he was in the slightest.

Anyway, I am sure you are wondering where is Eugene Debs during all
this, well he was not a delegate tot he congress but he condemned all
proposals to limit immigration. He declared the positions on immigration
that came from these congresses to be unsocialist and reactionary, and
said if any discrimination towards immigrants should happen it should
restrict from the most privileged countries, and fully encourage it from
the most exploited. [{% cite kipnis1952american %} 287]

However Debs generally thought party unity was more important and never
really mounted a fighting against the anti-asian and anti-black
sentiments in the party.

Now the Socialist Labor Party on the other hand was much more
pro-immigrant, however that does not mean they were not racist. They
thought opposing lynching was not a task for Socialists and that it was
a petite bourgeois concern.

Conclusion
==========

Well, thank you for watching, hope you find this useful at least with
showing the left has always considered issues of immigration important,
even though many reactionary socialists thought it was not an issue. The
Second International despite how chauvinistic it is still wouldn't
endorse anti-immigrant resolutions.

Anyway I kind of enjoyed taking a break talking about American Socialism
a bit, always something I hoped to cover too with this channel. Bit more
frustrating because somehow accessing documents and stuff can be more
difficult unless I am willing to drive around to country to look at
microfilms. Sorry this took so long to get out, I have been talking
about it a lot on my Twitter and just a mixture of getting sources
together and a bit of that lockdown depression have kept me from getting
it out as quickly.


# References

{% bibliography --cited %}
