---
layout: post
author:
- InDefenseOfToucans
bibliography: sources.bib
date: July 26 2020
title: 'Did Stalin "Steal" Trotsky''s Economic Program?'
---

<iframe width= "100%" height= "400px" sandbox="allow-same-origin allow-scripts allow-popups" src="https://watch.breadtube.tv/videos/embed/9faf94d7-7299-49a0-9c34-f3ec26792e76" frameborder="0" allowfullscreen></iframe>

[Click here for the PDF version of this script]({{ site.url }}/assets/scripts/stalinstealtrotskyeconomicprogram.pdf)

Introduction
============

There seems to be a persistent myth that Stalin took the programme of
Trotsky and the Left Opposition when he made his break with the NEP in
the late 20s. This incorrect history is very common it is even in George
Orwells Animal Farm, which given in at least most American schools it is
required reading might be where people get the idea. You may remember it
is the bit where the Stalin pig pisses on the plans for the windmill,
then ends up using them later and there is even a line about the Stalin
pig had his propganda pig guy announce he was the one who was for it all
along. This is supposed to represent the economic debates, but it is
wrong. Turns out this history is a bit more complicated then can be
explained with a windmill and pig piss.

The Myth
========

But this idea is not limited to an allegorical novella taught in high
schools as anti-communist propaganda. This actually became a pretty
prominent interpretation of events amongst western academic histories.
Stephan F. Cohen points this out in Bolshevism and Stalinism an Essay in
Robert C. Tucker's Stalinism Essays in Historical Interpretation.

>\"The programmatic debates of the 1920's are treated largely as an
extension of, and in terms of, the Trotsky-Stalin rivalry (or,
perpetuating the factional misnomers of the period, \"permanent
revolution\" and \"socialism in one country\"). Trotsky and the Left
opposition are said to have been anti-NEP and embronically Stalinist,
the progenitors of \"almost every major item in the political program
that Stalin later carried out.\" Stalin is then said to have stolen, or
adapted, Trotsky's economic policies in 1929. Having portrayed a \"basic
affinity between Trotsky's plan and Stalin's actions,\" these secondary
interpretations suggest at least a significant continuity between
Stalinism and Bolshevik thinking in the 1920's, and underlie the general
interpretation of NEP. They are, however, factually incorrect\"
[{% cite tucker1977stalinism %} 21]

This is the pretty common view on the left, like seriously look up
Trotsky and NEP, and you can find people arguing about it on Twitter.
Don't want to spend a ton of time on this, but the left seems so reliant
on like 70 year old historiography on the Soviet Union and constantly
repeat these older ideas. It also touches on the fact the debates of the
1920s weren't Permanent Revolution vs Socialism in One country, that is
a really bad inaccurate oversimplification, which I am not going to get
in fully here, but you should see a bit of why it is wrong by the end of
this.

Trotsky Originator of the NEP?
==============================

So why exactly is this theory wrong? Well Stalin's ending of the NEP
could not be taken from Trotsky as Trotsky was a proponent of the NEP
and even was one of the first to propose it. We can see Trotsky states
in My Life and The New course that in February of 1920 that he proposed
very similar policies to which would became the NEP. But just because
Trotsky said this in his sort of memoirs and an article from 1923 does
not mean we can trust it. Now I don't have the full transcript of the
10congress, but we can see from two historians that Trotsky at this
congress made a point of mentioning that he did propose this and no one
challenged that or said it was wrong.

From Moshe Lewin's Political Undercurrents in Soviet Economic Debates

>\"Trotsky, who adopted wholeheartedly the NEP at its inception\"

Then from a note at the bottom of the page

>\"Trotsky could afford to endorse the NEP wholeheartedly because he too
had some previous positions to call back on. He was , in fact, the first
to have advocated NEP-like changes as early as Februrary 1920, but his
proposals were then rejected by the Central committee. Trotsky then
turned to his plan of statization of the trade unions, but this too was
rejected by Lenin, who was soon to adopt the NEP (on this both leaders
agreed).\"[ {% cite lewin1974political %} 93]

and from E.H Carr's Volume 2 of his A History of Soviet Russia.

>\"In February 1920, before the ninth party congress, at a moment when
the civil war already seemed over, Trotsky had proposed in the Politburo
to replace requisitioning of surpluses by a tax in kind calculated on a
percentage of production, and to put the exchange of goods with the
peasantry on an individual rather then a collective basis. But he had
been opposed by Lenin, and obtained only 4 of the 15 votes.\"
[ {% cite carr1978bolshevik %} 280]

Now with the opening of the archives we did find it Trotsky was not the
first person to bring up a replacement of grain requisitions with a tax
and permit a certain level of trade. Around the same time but unknown to
Trotsky, Yuri Larin just a week or two prior proposed something similar
which was supported by Rykov, but was opposed by Lenin. Now none of this
was a new idea, before the civil war broke out there was talk of
taxation and trade with the peasantry, but this never really took
effect. [{% cite nove1992economic %} 69]

Of course too we don't even really have to trust Trotsky or Historians
that Trotsky supported the NEP essentially out of the gate. In Lenin's
To the Russian Colony in North America, Lenin says to anyone questioning
the NEP \"I would refer to the speeches of Comrade Trotsky and my own
speech at the Fourth Congress of the Communist International\" Trotsky's
speech in question was delivered at Session 10, on Tuesday the 14of
November of 1922 at 6:15 pm, a summary can be found on Marxists.org
under the title \"The Economic Situation of Soviet Russia from the
Standpoint of the Socialist Revolution\" and Lenin's is Session 8 on
Monday 13of November of 1922 at 11:40 am. This speech was Lenin's first
public one following a stroke in May 1922, it is rather short compared
to Trotsky's on the question. So we have Lenin saying that Trotsky's
speech was a defense of the NEP and refereed people to it.

But you might say \"well Trotsky initially was for it, but him and the
Left Opposition became opponents of it after Lenin's death\", but this
simply is not true either.

Trotsky, The Left Opposition and the NEP
========================================

Pulling from Cohen's article again

>\"Trotsky's actual economic proposals in the 1920s were based on the NEP
and its continuation. He urged greater attention to heavy industry and
planning earlier than did Bukharin, and he worried more about the
village \"kulak\"; but his remedies were moderate, market-orientated,
or, as the expression went, \"nepist.\" Like Bukharin, he was a
\"reformist\" in economic policy, looking towards the evolution of NEP
Russia towards industrialism and socialism.\" [{% cite tucker1977stalinism %} ]

Now two quotes from Moshe Lewin's Political Undercurrents in Soviet
Economic Debates

>\"They envisaged the continuation of the NEP and therefore logically
enough, stated that although they intended to exercise greater control
over the kulaks and private entrepreneurs, to tax them more efficient,
and to promote more collectivization in the countryside, the liquidation
of the kulaks and of private sectors, or a large-scale administrative
drive against peasants,w as also out of the question.\"
[{% cite lewin1974political %} 16]

>\"In propaganda texts, the majority's spokesmen accused the Left of
planning to liquidate the NEP, to oppress the peasantry, to raise prices
and lower the standard of living, and other sins. But the latter, no
doubt sincerely, reasserted that it favored the NEP, did not intend to
expropriate the property of the kulaks, nor indeed, that of any other
private entrepreneurs, and that it, in fact even, welcomed some growth
of these elements provided the growth of the socialist sector, mainly
industrial, was constantly assured. They opposed using the G.P.U.
against the private sectors.\" [{% cite lewin1974political %} 35]

In terms of economic thinking the Left Argued for development of the
industrial sector and that collectivization had to follow the
development of the industrial sector, and that this should be done with
the peasants consent, and that even the agricultural industry could grow
so as long as industry was growing faster and could keep up to supply
them with goods to keep the relationship good. [{% cite lewin1974political %} 36]

So we can see the idea that Trotsky rejected the NEP is false. I also
see ideas that Trotsky was against development of the Soviet Economy,
when it is really the opposite, Trotsky represented the faction calling
for industrialization and focus on economic development. This is in
opposition to Bukharin's position of industrialization at a snails pace.

Stalin's economic position before collectivization
==================================================

Another thing that is not often enough pointed out, Stalin was not the
leading figure opposed to Trotsky on economimcs, that was Bukharin.

>\"Stalin's public policies on industry, agriculture and planning were
Bukharin's, that is, pro-NEP, moderate, evolutionary. This was the
cement of the Stalin-Bukharin duumvirate that made official policy and
led the party majority against the Left opportunists until early 1928.\"
[{% cite tucker1977stalinism %} 21-22]

Though in private Stalin would often show more of a disagreement with
Bukharin, \"In June of the same year, he firmly declared behind the
scenes that Bukharin's slogan 'enrich yourselves', which he had
addressed to 'all the peasants', was 'not our slogan' and
'incorrect'\"[{% cite davies2005stalin %} 122]

End of the NEP
==============

When Stalin ended the NEP he was not \"stealing\" anyone economic plans,
not his former ally Bukharin, nor his opponent Trotsky. Stalin created
his new policy regardless of if you think it was justified or not it was
a break from previous Bolshevik thinking. From Cohen again \"Stalin's
new policies of 1929-33, the \"great change,\" were a radical depature
from Bolshevik programmatic thinking. No Bolshevik leader or faction had
ever advocated anything adkin to imposed collectivization,l the
\"liquidation\" of the kulaks, breakneck heavy industrialization\"
[{% cite tucker1977stalinism %} 24]

Conclusion
==========

Now I really could have just used Cohen's whole essay because he
essentially makes the same argument, but given it is from one guy would
make it easier for some people to dismiss. Plus I think the quotes from
Lenin aid this, something else I want to mention. But a sort of appeal
not to trust memoirs I think this is maybe another major source of this
myth. Harry Haywood actually says Trotsky attacked the NEP from the
start in his memoirs, when this is just really flat out wrong and I
think it is fine and even good to read books like this, but they
shouldn't be your only source on things. In this video I didn't take
Trotsky at his word in recalling the history I went and verified that
claim with more then one historian.

Hope you enjoyed this video and it helps you understand some of the
positions of the figures in the 1920s Soviet Economic Debates, I have a
much longer video showing Bukharin's positions during this period coming
some time soon. Hopefully after my next video I will be done with my
break from longer videos and I will release some longer big history

{% bibliography --cited %}
