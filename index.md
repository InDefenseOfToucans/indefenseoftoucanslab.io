---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
title: Home
---
Welcome to my website where I post scripts and whatever else I decide on for my channel InDefenseOfToucans

## My [Peertube](https://watch.breadtube.tv/accounts/indefenseoftoucans/video-channels)

## My [Youtube](https://www.youtube.com/c/InDefenseOfToucans)
